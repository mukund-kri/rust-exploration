# Basic Postgres

## Aim

In this project I aim to understand the basics of using the `postgres` crate to 
interact with a postgres database.

## Crates Used

1. `postgres` - The main crate used to interact with the postgres database.

## Binaries

1. `create` :: Creates a table called person
2. `insert_read` :: Shows simple write/read into a DB

