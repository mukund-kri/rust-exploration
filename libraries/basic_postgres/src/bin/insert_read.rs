/// Insert and read from db
use postgres::{Client, NoTls};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = Client::connect(
        "host=localhost user=population password=secret dbname=population",
        NoTls,
    )?;

    let age: i16 = 30;
    client.execute(
        "INSERT INTO person (first_name, last_name, age) VALUES ($1, $2, $3)",
        &[&"John", &"Doe", &age],
    )?;

    let query = client.query("SELECT first_name, last_name, age FROM person", &[])?;
    for row in &query {
        let first_name: &str = row.get(0);
        let last_name: &str = row.get(1);
        let age: i16 = row.get(2);
        println!("Found person {} {} with age {}", first_name, last_name, age);
    }
    Ok(())
}
