/// The very basics of using the `postgres` crate.
use postgres::{Client, NoTls};

fn main() {
    // Connect to the database.
    let mut client = Client::connect(
        "host=localhost user=population password=secret dbname=population",
        NoTls,
    )
    .unwrap();

    // Create a table.
    let create_sql = "CREATE TABLE person (
        id              SERIAL PRIMARY KEY,
        first_name      VARCHAR NOT NULL,
        last_name       VARCHAR NOT NULL,
        age             SMALLINT
    )";

    client.batch_execute(create_sql).unwrap();
}
