use std::error::Error;
use std::path::Path;
use std::process::exit;

const SCAN_ROOT: &str = "/run/user/1000/gvfs/mtp:host=Xiaomi_22127PC95I_5d9pkrcmpbpzvgs4/Internal shared storage/Android/data/com.indymobileapp.document.scanner/files/ClearScanner";

#[derive(Debug)]
enum PageType {
    Front,
    Back,
}

#[derive(Debug)]
struct Scan {
    id: String,
    path: String,
    page_type: PageType,
}

fn main() -> Result<(), Box<dyn Error>> {
    // Create a vector to store the scans
    let mut scans: Vec<Scan> = Vec::new();

    if !Path::new(SCAN_ROOT).exists() {
        println!("Your phone is not connected. Connect your phone and try again.");
        exit(1);
    }

    // List all the directories in the SCAN_ROOT
    let entries = std::fs::read_dir(SCAN_ROOT)?;

    let mut is_front = true;

    for entry in entries {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            let id = path.file_name().unwrap().to_str().unwrap().to_string();
            scans.push(Scan {
                id,
                path: path.to_str().unwrap().to_string(),
                page_type: if is_front {
                    PageType::Front
                } else {
                    PageType::Back
                },
            });

            is_front = !is_front;
        }
    }

    // Print the scans
    for scan in scans {
        println!(
            "ID: {}, Page Type: {:?}, Path: {}",
            scan.id, scan.page_type, scan.path
        );
    }

    Ok(())
}
