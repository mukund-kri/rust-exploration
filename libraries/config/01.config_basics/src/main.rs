use std::error::Error;
use std::fmt::Display;
use std::path::Path;

// Config is the center of config crate
use config::Config;
use serde_derive::Deserialize;

const SETTINGS_PATH: &str = "/home/mukund/.config/noteapp/Settings.toml";

// One potential error is that either the note or scan path don't exist
#[derive(Debug)]
struct SettingsValidationError {
    message: String,
}

impl Display for SettingsValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for SettingsValidationError {}

#[derive(Debug, Deserialize)]
struct Basic {
    notes_path: String,
    scans_path: String,
}

impl Basic {
    fn validate(&self) -> Result<(), SettingsValidationError> {
        // Check if the notes path exists
        if !Path::new(&self.notes_path).exists() {
            return Err(SettingsValidationError {
                message: format!("Notes path {} does not exist", self.notes_path),
            });
        }
        // Check if the scans path exists
        if !Path::new(&self.scans_path).exists() {
            return Err(SettingsValidationError {
                message: format!("Scans path {} does not exist", self.scans_path),
            });
        }
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    // create and build a new Config object
    let settings = Config::builder()
        // Make `Settings.toml` as the only source of configuration
        .add_source(config::File::with_name(SETTINGS_PATH))
        .build()?;

    // Get the value of notes_path from the settings
    let notes_path = settings.get::<String>("notes_path")?;
    println!("Notes path: {}", notes_path);

    // try to deserialize the settings into a Basic struct
    let basic: Basic = settings.try_deserialize::<Basic>()?;
    println!("Full config: {:?}", basic);

    // Validate the settings
    match basic.validate() {
        Ok(_) => println!("Settings are valid"),
        Err(e) => eprintln!("Error: {}", e),
    }

    Ok(())
}
