// Explore regex in rust
use regex::Regex;

fn main() {
    // Simple match
    let re = Regex::new(r"^\d{4}-\d{2}-\d{2}$").unwrap();
    let date = "2020-01-01";
    if re.is_match(date) {
        println!("Date is valid");
    } else {
        println!("Date is invalid");
    }

    // Capturing groups
    let re = Regex::new(r"(\d{4})-(\d{2})-(\d{2})").unwrap();
    let date = "2020-01-01";
    let captures = re.captures(date).unwrap();
    println!("Year: {}", &captures[1]);
    println!("Month: {}", &captures[2]);
    println!("Day: {}", &captures[3]);

    // Now with named groups
    let re = Regex::new(r"^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$").unwrap();
    let date = "2020-01-01";
    let captures = re.captures(date).unwrap();
    println!("Year: {}", &captures["year"]);
    println!("Month: {}", &captures["month"]);
    println!("Day: {}", &captures["day"]);
}
