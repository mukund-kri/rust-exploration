/// Exploring how build parsers with `derive` API
use clap::Parser;

/// A simple parser
#[derive(Parser)]
#[command(name = "derive")]
#[command(author = "Mukund K")]
#[command(version = "0.1.0")]
#[command(about = "A simple parser")]
struct SimpleParser {
    /// Positional one
    #[arg(long)]
    one: String,

    /// Positional two
    #[arg(long)]
    two: String,
}

fn main() {
    let parser = SimpleParser::parse();

    println!("one: {}", parser.one);
    println!("two: {}", parser.two);
}
