use std::path::PathBuf;

use clap::{Parser, Subcommand};

/// Exploring how to write write clap app with the derive API
/// With the derive API, we use a struct and attributes to define how the cli parameters should be
/// parsed

/// clap supports subcommands, which is defined as a enum
#[derive(Subcommand)]
enum Commands {
    /// Test subcommand
    Test {
        #[arg(short, long)]
        list: bool,
    },
}

/// This app servers as demo for the derive API
#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Cli {
    /// Name, optional argument
    name: Option<String>,

    /// config_file.
    #[arg(short, long, value_name = "FILE")]
    config_file: Option<PathBuf>,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    /// Subcommands configuration
    #[command(subcommand)]
    command: Option<Commands>,
}

fn main() {
    let cli = Cli::parse();

    if let Some(name) = cli.name.as_deref() {
        println!("Hello, {}!", name);
    } else {
        println!("Hello, world!");
    }

    if let Some(config_file) = cli.config_file {
        println!("Config file: {:?}", config_file);
    }

    match cli.verbose {
        0 => println!("Not verbose"),
        1 => println!("Verbose"),
        2 => println!("Very verbose"),
        3 => println!("Debug mode"),
        _ => println!("Max verbose"),
    }

    match &cli.command {
        Some(Commands::Test { list }) => {
            if *list {
                println!("Printing testing lists...");
            } else {
                println!("Not printing testing lists...");
            }
        }
        None => {}
    }
}
