use std::path::PathBuf;

use clap::{arg, command, value_parser, Command};

fn main() {
    let matches = command!()
        .arg(arg!([name] "Optional name of the person to greet"))
        .arg(
            arg!(
                -c --config <FILE> "Sets a custom config file"
            )
            .required(false)
            .value_parser(value_parser!(PathBuf)),
        )
        .arg(
            arg!(
                -d --debug "Print debug information verbosely"
            )
            .required(false),
        )
        .subcommand(
            Command::new("test")
                .arg(
                    arg!(
                        -l --list "List all available tests"
                    )
                    .required(false),
                )
                .arg(
                    arg!(
                        -t --test <TEST> "Run a specific test"
                    )
                    .required(false)
                    .value_parser(value_parser!(String)),
                ),
        )
        .get_matches();

    println!("{:?}", matches);
}
