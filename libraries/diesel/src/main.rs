#[macro_use]
extern crate diesel;

mod db;
mod models;
mod schema;

use crate::models::{NewVideo, Video};
use db::establish_connection;
use diesel::prelude::*;

fn create_new_video() {
    use crate::schema::videos::dsl::*;

    let new_video = NewVideo {
        title: "A new video",
        description: "Description of that new video",
        removed: false,
    };

    let mut conn = establish_connection();

    diesel::insert_into(videos)
        .values(&new_video)
        .execute(&mut conn)
        .expect("Failed to insert into new connection");
}

fn main() {
    create_new_video();
}
