use std::error::Error;

use async_std::stream::StreamExt;
use sqlx::postgres::PgPoolOptions;

// Each row

#[derive(Debug, PartialEq, Eq, sqlx::FromRow)]
struct Todo {
    id: i32,
    title: String,
    completed: bool,
}

// main is async and runs via async-std
#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Connect to the database
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://devuser:devpass@localhost:5432/devdb")
        .await?;

    // Load data
    query_data_checked(&pool).await?;

    Ok(())
}

// Function to load some data into the todo table
async fn load_data(pool: &sqlx::PgPool) -> Result<(), sqlx::Error> {
    // Load some data into the todo table
    sqlx::query("INSERT INTO todos (title, completed) VALUES ($1, $2)")
        .bind("Implement SQLx")
        .bind(false)
        .execute(pool)
        .await?;

    Ok(())
}

// Function to query the todo table
async fn query_data(pool: &sqlx::PgPool) -> Result<(), sqlx::Error> {
    let result = sqlx::query_as::<_, Todo>("SELECT * FROM todos")
        .fetch_all(pool)
        .await?;

    for todo in result {
        println!("{:#?}", todo)
    }

    Ok(())
}

// SQL compiled checked
async fn query_data_checked(pool: &sqlx::PgPool) -> Result<(), sqlx::Error> {
    let mut result = sqlx::query_as!(Todo, "SELECT * FROM todos").fetch(pool);

    while let Some(Ok(todo)) = result.next().await {
        println!("title: {:#?}", todo)
    }
    Ok(())
}
async fn query_data2(pool: &sqlx::PgPool) -> Result<(), sqlx::Error> {
    let todos = sqlx::query!("SELECT * FROM todos").fetch_all(pool).await?;

    for todo in todos {
        println!("id: {}, title: {}", todo.id, todo.title);
    }

    Ok(())
}
