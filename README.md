# rust-exploration

Code I have written while taking a look at rust.

## `language` folder

Contains code I have (mostly) written while reading the [Rust Book](https://doc.rust-lang.org/book/).

## `stdlib` folder
Exploring the rust standard library.

## `libraries` folder

Exploring some of the more popular crates.