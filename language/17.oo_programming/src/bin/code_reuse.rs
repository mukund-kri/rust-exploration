/// The OO style code reuse constructs are limited in rust.
/// Default trait methods are one way to reuse code in rust.

/// Trait with default method.
/// The default method can be overridden by the implementor.
trait Animal {
    fn name(&self) -> &'static str;

    fn talk(&self) {
        println!("{} cannot talk", self.name());
    }
}

/// Implement the trait for Dog.
struct Dog {
    name: &'static str,
}

impl Animal for Dog {
    fn name(&self) -> &'static str {
        self.name
    }
}

/// Implement the trait for Cat.
struct Cat {
    name: &'static str,
}

impl Animal for Cat {
    fn name(&self) -> &'static str {
        self.name
    }
}

// At this  point notice how the talk method is reused by both Dog and Cat.

fn main() {
    let dog = Dog { name: "Rover" };
    let cat = Cat { name: "Misty" };

    dog.talk();
    cat.talk();
}
