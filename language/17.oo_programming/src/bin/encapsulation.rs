/// Let's explore how we do encapsulation in Rust. Basically, we use the `pub` keyword in
/// conjunction with modules to achieve encapsulation.

mod complex {

    /// `Complex` is a struct that represents a complex number.
    /// Here we use the `pub` keyword to make the struct public but its fields private and so
    /// cannot be accessed outside the module. This is a effective way to achieve encapsulation.
    pub struct Complex {
        real: f64,
        imag: f64,
    }

    // The so called `API` is marked as `pub` and can be accessed outside the module.
    impl Complex {
        pub fn new(real: f64, imag: f64) -> Complex {
            Complex { real, imag }
        }

        pub fn add(&self, other: &Complex) -> Complex {
            Complex {
                real: self.real + other.real,
                imag: self.imag + other.imag,
            }
        }

        pub fn sub(&self, other: &Complex) -> Complex {
            Complex {
                real: self.real - other.real,
                imag: self.imag - other.imag,
            }
        }

        pub fn mul(&self, other: &Complex) -> Complex {
            Complex {
                real: self.real * other.real - self.imag * other.imag,
                imag: self.real * other.imag + self.imag * other.real,
            }
        }

        pub fn div(&self, other: &Complex) -> Complex {
            let denom = other.real * other.real + other.imag * other.imag;
            Complex {
                real: (self.real * other.real + self.imag * other.imag) / denom,
                imag: (self.imag * other.real - self.real * other.imag) / denom,
            }
        }

        pub fn to_string(&self) -> String {
            format!("{} + {}i", self.real, self.imag)
        }
    }
}

fn main() {
    // Let's create a complex number.

    // We can't do this because the fields are private.
    // let c = complex::Complex { real: 1.0, imag: 2.0 };

    // We have to use the API. This mean the new associated function.
    let c = complex::Complex::new(1.0, 2.0);
    println!("c = {}", c.to_string());

    // access to the implementation can be blocked
    // Following line will not compile
    // c.real = 3.0;
    // println!("c = {}", c.real);

    // Let's play the other API
    let d = complex::Complex::new(3.0, 4.0);

    let e = c.add(&d);
    println!("c + d = {}", e.to_string());

    let f = c.sub(&d);
    println!("c - d = {}", f.to_string());

    let g = c.mul(&d);
    println!("c * d = {}", g.to_string());

    let h = c.div(&d);
    println!("c / d = {}", h.to_string());
}
