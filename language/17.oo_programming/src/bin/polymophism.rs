/// Let's explore polymorphism in Rust.
/// It's done with traits.
/// This example si from the book

/// The Draw trait defines how a component/widget will draw itself on the screen.
pub trait Draw {
    /// The draw method takes a mutable reference to self, and returns nothing.
    /// All implementors of the Draw trait must define how they will draw themselves on the screen
    /// here.
    fn draw(&self);
}

/// Represents the full window of the GUI.
pub struct Screen {
    // Note: the `dyn` keyword is used to indicate that the type is a trait object.
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

// Let's make some structs that implement the Draw trait

/// A button widget. Note it implements the Draw trait.
pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        println!(
            "Drawing a button with width {}, height {} and label {}",
            self.width, self.height, self.label
        );
    }
}

/// A select box widget. Note this also implements the Draw trait.
pub struct SelectBox {
    pub width: u32,
    pub height: u32,
    pub options: Vec<String>,
}

impl Draw for SelectBox {
    fn draw(&self) {
        println!(
            "Drawing a select box with width {}, height {} and options {:?}",
            self.width, self.height, self.options
        );
    }
}

fn main() {
    // Lets use our imaginary GUI library to draw out a simple screen
    let screen = Screen {
        // The trait objects in the vector are of different types, but they all implement the Draw
        // trait.
        components: vec![
            Box::new(SelectBox {
                width: 75,
                height: 10,
                options: vec![
                    String::from("Yes"),
                    String::from("Maybe"),
                    String::from("No"),
                ],
            }),
            Box::new(Button {
                width: 50,
                height: 10,
                label: String::from("OK"),
            }),
        ],
    };

    screen.run();
}
