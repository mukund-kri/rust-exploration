/// A demo of the state pattern, written in Rust.
/// This example is from the book, and is intended as a demonstration of using OO techniques in
/// Rust.
use oo_programming::post::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());
}
