# OO programming in Rust

Rust is not classified as an Object oriented language, but it does have some OO features
or constructs that do the same thing.

In this chapter we'll explore ...
1. Encapsulation
2. Inheritance (or rust's way of doing it)
3. Polymorphism

## Encapsulation

Example :: [encapsulation.rs](./src/bin/encapsulation.rs)
Encapsulation is the idea of hiding the implementation details of a data structure from 
the user. In rust, this is done by using the `pub` keyword. Here we code the 
`struct`/`enum` and the `impl` block and mark public facing API with `pub` keyword.

## Inheritance

Example :: [inheritance.rs](./src/bin/code_reuse.rs)

Rust **does not** have inheritance. 

Most of the time in a full OO language we use inheritance for ...
1. Code reuse
2. Polymorphism

Rust's traits provide a limited form of code reuse, where the trait can have a default
implementation. This is similar to the default implementation of an interface in Java.
and can be overridden by the implementing type.
That's all, but that covers most of the use cases for code reuse via. inheritance.

## Polymorphism

Example :: [polymorphism.rs](./src/bin/polymorphism.rs)

Rust uses a combination of traits and generics to achieve polymorphism.

