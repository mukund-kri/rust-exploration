# Concurrency in Rust with threads

The Rust standard library provides a thread API that is unlike any other low-level
languages quite safe to use. The concept of ownership and borrowing is used to
ensure that threads are safe to use.

## files

1. `src/bin/basics.rs` :: basic thread example
2. `src/bin/message_passing.rs` :: sharing state between threads via. message passing
3. `src/bin/mutex.rs` :: sharing state between threads via. mutexes