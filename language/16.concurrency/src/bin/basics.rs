/// Create and run code in separate thread
use std::thread;
use std::time::Duration;

fn main() {

    println!("Basics of creating and spawning threads ....");
    // Run code in separate thread
    let handle = thread::spawn(|| {
        for i in 1..11 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    // Wait for spawned thread to finish before running the next code
    handle.join().unwrap();

    // Run the same code in main thread
    for i in 1..6 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    // Wait for spawned thread to finish
    //handle.join().unwrap();

    println!("Ownership and threads ...");

    let v = vec![1, 2, 3];

    // Following code will not compile because the spawned thread may outlive the vector v
    // let handle = thread::spawn(|| {
    //     println!("Here's a vector: {:?}", v);
    // });
    // handle.join().unwrap();

    // The solution is to move the ownership of v to the spawned thread
    let handle = thread::spawn(move || {
        println!("Here's a vector: {:?}", v);
    });
    handle.join().unwrap();
}
