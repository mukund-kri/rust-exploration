/// The mutex is one of the primitives for sharing memory across threads.
/// The idea here is that only one thread is given access to the memory at any given time.
use std::{
    sync::{Arc, Mutex},
    thread,
};

/// Simple mutex use example
fn simple_mutex() {
    // create a mutex on an int
    let m = Mutex::new(5);

    // Let's modify the value in the mutex. This involves getting a lock on the mutex and then
    // modifying the data
    {
        let mut num = m.lock().unwrap();
        *num = 6;
    }

    println!("m = {:?}", m);
}

/// Multiple ownership with Multiple threads
/// For this we use the Arc<T> (Atomic Reference Counting) smart pointer.
fn multi_ownership() {
    // In this case we wrap the Mutex in a Arc
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    // Let's create 10 threads the update the mutex
    for _ in 0..10 {
        // First let's clone the mutex as we will give ownership of this mutex to the thread
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            // Get the data stored in the mutex
            let mut count = counter.lock().unwrap();
            *count += 1;
        });

        handles.push(handle);
    }

    // Wait for all the threads to finish
    for handle in handles {
        handle.join().unwrap();
    }

    // Finally read the mutex in the main thread
    println!("Result :: {}", *counter.lock().unwrap());
}

fn main() {
    simple_mutex();
    multi_ownership();
}
