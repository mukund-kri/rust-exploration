#[allow(unused)]
use std::sync::mpsc;
/// Let's explore message passing between rust threads
use std::thread;
use std::time::Duration;

fn basic_send_receive() {
    let (tx, rx) = mpsc::channel();

    // Let's create a new thread and send a message from this thread
    thread::spawn(move || {
        let message = String::from("Hello!");
        tx.send(message).unwrap();
    });

    // Let's receive messages on the main thread
    let received = rx.recv().unwrap();
    println!("Got :: {}", received);
}

fn multiple_send() {
    let (tx, rx) = mpsc::channel();

    // Let's create a thread and send messages from there
    thread::spawn(move || {
        let messages = vec!["hello", "from", "the", "thread"];

        for m in messages {
            tx.send(m.to_string()).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // Wait for the messages in the main thread
    for received in rx {
        println!("Got :: {}", received);
    }
}

/// mpsc is an acronym for `multiple producer, single consumer`. In this example we see how
/// multiple producers can send messages to a single consumer.
/// This is accomplished by cloning the transmitter.
fn multiple_transmitters() {
    let (tx0, rx) = mpsc::channel();

    // Let's make one more transmitter by cloning the one above
    let tx1 = tx0.clone();

    // Let's make a new thread which uses tx0 to send messages
    thread::spawn(move || {
        let messages = vec!["hi", "from", "thread", "one"];

        for m in messages {
            tx0.send(m.to_string()).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // Let's make another thread that uses tx1 to send messages
    thread::spawn(move || {
        let messages = vec!["more", "messages", "from", "thread"];

        for m in messages {
            tx1.send(m.to_string()).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // Let's receive the messages from the above threads
    for received in rx {
        println!("Got :: {}", received);
    }
}

fn main() {
    // basic_send_receive();
    // multiple_send();
    multiple_transmitters();
}
