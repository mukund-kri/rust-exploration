# the IO project

A project with concepts from chapters 1 - 11 of the book.

Implement a simple command line tool that searches for a string in a file and prints the lines that contain the string.