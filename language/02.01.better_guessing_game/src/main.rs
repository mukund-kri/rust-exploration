use rand::Rng;
use std::cmp::Ordering;
use std::error::Error;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    // Let's start of with generating a random number between 0 and 100
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Enter a number: ");

        // Declare and read in user's guess
        let mut guess = String::new();
        io::stdin().read_line(&mut guess)?;

        // Convert guess to int, prompt user if not an int
        let guess = match guess.trim().parse::<i32>() {
            Ok(guess) => guess,
            Err(_) => {
                println!("Not a number, please try again");
                continue;
            }
        };

        // Compare number to target
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("You Win");
                break;
            }
        };
    }

    // Return to make the typechecker happy
    Ok(())
}
