# Slight improved Guessing Game

## Original

This example is from the book, [The Rust Programming Language](https://doc.rust-lang.org/book/second-edition/).

Simple guessing game in Rust. Demonstrates a simple program with user input, random numbers, and a loop.

## Improvements

1. Box<dyn Error> for more ergonomic error handling
2. Demo use of `turbofish` syntax