/// Explore CONTROL FLOW in rust
///
/// Note: both `if` and loops are expressions in rust ie. they "return" a value.
///

fn main() {
    println!("EXPLORING IF EXPRESSIONS ...");
    let num = 3;

    // Basic syntax for the if-else expression
    if num < 5 {
        println!("The condition is true");
    } else {
        println!("The condition if false");
    }

    // Another form that does the same as above but, this time using the "return value"
    // of the expression
    let message = if num < 5 {
        "The condition is true"
    } else {
        "The condition is false"
    };
    println!("{}", message);

    // Rust does not do "truthy-ness" like other languages. The following will not compile
    // let num = 3;
    // if num {
    //     println!("Number was three");
    // }

    // The way to do multiple conditions is with `else if` like c like languages
    // example from the book
    let num = 6;

    if num % 4 == 0 {
        println!("The number is divisible by 4");
    } else if num % 3 == 0 {
        println!("The number is divisible by 3");
    } else if num % 2 == 0 {
        println!("The number is divisible by 2");
    } else {
        println!("The number is not divisible by 4, 3, or 2");
    }

    // Or the expression version
    let message = if num % 4 == 0 {
        "The number is divisible by 4"
    } else if num % 3 == 0 {
        "The number is divisible by 3"
    } else if num % 2 == 0 {
        "The number is divisible by 2"
    } else {
        "The number is not divisible by 4, 3, or 2"
    };
    println!("{}", message);

    println!("EXPLORING LOOPS ...");

    // The basic loop
    // This produces an infinite loop, hence it is commented out
    // loop {
    //     println!("again!");
    // }

    // Exiting from loops is done with `break` and `continue` constructs
    // print down to 0, but only the even numbers
    let mut counter = 10;
    loop {
        if counter == 0 {
            // break out of the loop completely
            break;
        } else if counter % 2 == 0 {
            println!("The counter is {}", counter);
        } else {
            // Not the best way to do this, but showing how to use `continue`
            // skip one iteration
            counter -= 1;
            continue;
        }
        counter -= 1;
    }

    // Loops can be labeled and then the label can be used with `break` and `continue`
    // The following is an example is rather contrived, but it shows the syntax
    let i_max = 5;
    let j_max = 4;

    let mut i = 0;
    let mut j = 0;

    'j_loop: loop {
        'i_loop: loop {
            if i == i_max {
                break 'i_loop;
            }
            if j == j_max {
                break 'j_loop;
            }
            println!("i: {} j: {}", i, j);
            i += 1;
        }
        i = 0;
        j += 1;
    }

    // Loops are also expressions
    let mut sum = 0;
    let mut up_to = 10;

    let result = loop {
        sum += up_to;
        up_to -= 1;
        if up_to == 0 {
            break sum;
        }
    };
    println!("The sum is {}", result);
    println!();

    // While loops
    println!("EXPLORING WHILE LOOPS ...");

    // Also supported is the `while` loop. Here the condition is checked before the loop
    // body is executed. So, no fiddling with break and continue.
    let mut number = 3;
    while number != 0 {
        println!("{}!", number);
        number -= 1;
    }

    // The for loop is built specifically for iterating over collections
    println!("EXPLORING FOR LOOPS ...");
    let a = [10, 20, 30, 40, 50];
    for ele in a {
        println!("The value is {}", ele);
    }
}
