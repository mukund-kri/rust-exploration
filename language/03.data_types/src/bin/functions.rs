/// Exploring functions in rust
///
/// Functions are rather simple in rust. The following features do not exist ...
/// 1. Default parameters
/// 2. Named parameters
/// ...

/// A function that adds two numbers together
fn add(a: i32, b: i32) -> i32 {
    // implicitly return the value of the last expression
    // DON'T end the return expression with semi-colon
    a + b
}

/// first class function can be done with `Function Pointers`
fn do_opp(f: fn(i32, i32) -> i32, param1: i32, param2: i32) {
    let ans = f(param1, param2);
    println!("The result of call is {}", ans);
}

fn main() {
    // Simple function call
    let sum = add(2, 54);
    println!("The sum is :: {sum}");

    // Pass function into function
    do_opp(add, 2, 4);
}
