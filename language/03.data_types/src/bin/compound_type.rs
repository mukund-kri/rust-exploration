use std::io;

/// Code mostly from Chapter 3 of the rust book

fn main() {
    println!("EXPLORING TUPLES ...");
    // Tuples are a fixed length, and can have different type elements
    let tup: (i32, f64, char) = (24, 93.7, 'z');
    println!("The Tuple {:?}", tup);

    // Tuples can also be deconstructed
    let (x, y, z) = tup;
    println!("The deconstructed values are {x} {y} {z}");

    // We can also access the individual values by the index as follows ...
    let first_ele = tup.0;
    let second_ele = tup.1;
    let third_ele = tup.2;

    println!("First: {first_ele} Second: {second_ele} Third: {third_ele}");

    // Mutable tuples
    let mut tup2 = (10, 20, 'z');
    println!("Before mutation {:?}", tup2);
    tup2.1 = 40;
    println!("After mutation {:?}", tup2);
    println!();

    println!("EXPLORING ARRAYS ...");
    // ARRAYS
    // - collection of multiple values
    // - the elements have to be the same type
    // - it is fixed length ie. once declared the size of the array cannot change

    // Creating a new array
    let arr = [2, 45, 23, 34, 2];

    // As with most languages the elements can be accessed with a 0 based index
    println!("The 1st element is {}", arr[1]);

    // Another thing to note is the index out of bounce `runtime error`
    // Example from the book
    println!("Please enter a index ...");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    // convert to int
    let index: usize = index.trim().parse().expect("Index was not a number");

    let element = arr[index];

    println!("The target element is :: {}", element);
}
