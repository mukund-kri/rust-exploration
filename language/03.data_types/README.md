# Common Programming Concepts

This project explores the `Common Programming Concepts` chapter of the Rust Book.


## Data Types

The code is in  `src/bin/data_types.rs`.

## Functions

The code is in `src/bin/functions.rs`.

## Control Flow

The code is in `src/bin/control_flow.rs`.