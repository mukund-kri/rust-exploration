use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // Let's start of with generating a random number between 0 and 100
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Enter a number: ");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Could not read the your input");

        let guess: u32 = guess.trim().parse().expect("Pease enter a number");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("You Win");
                break;
            }
        };
    }
}
