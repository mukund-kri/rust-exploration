# Patterns and Matching

In this project, we do a deep dive into rust patterns and matching.
Patterns can be divided into two categories: refutable and irrefutable.

## Irrefutable Patterns
Irrrefutable patterns are patterns that SHOULD match the value, otherwise the program 
will fail to compile.
These constructs work on irrefutable patterns:
1. `let` statements
2. `for` loops
3. `function` parameters

For example, the following code will fail to compile:
```rust
let Some(x) = None;
```
Pattern on the left does not match the value on the right and hence not irrefutable.

## Refutable Patterns

Refutable patterns MAY or MAY NOT match the value. So, is ideal for branching code.
These constructs work on refutable patterns:
1. `if let` expressions
2. `while let` loops
3. `match` expressions

## Files

1. [destructuring.rs](src/bin/destructuring.rs) :: Irrefutable patterns (basically destructuring) examples
2. [matching.rs](src/bin/matching.rs) :: Examples of the `match` expression
3. [if_while_let.rs](src/bin/if_while_let.rs) :: Examples of the `if let` expression and `while let` loop
4. [for_functions.rs](src/bin/for_functions.rs) :: Examples of destructring in the `for` loop and `function` parameters