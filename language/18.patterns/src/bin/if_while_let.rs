/// Let's explore the `if let` and `while let` constructs

fn main() {
    println!("\nIf let examples ...");
    // if let is a shorthand for a match that only matches one case
    let option1 = Some(5);
    if let Some(x) = option1 {
        println!("x is {}", x);
    }

    // `if let` also comes with an else branch
    let option2: Option<i32> = None; // Note: explicit type annotation as compiler can't infer type from None
    if let Some(x) = option2 {
        println!("x is {}", x);
    } else {
        println!("option is None");
    }

    println!("\nWhile let examples ...");

    // If we want to do iteration with pattern matching, on each iteration, we can use `while let`
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);

    // This is the idiomatic way of popping elements from a stack
    while let Some(top) = stack.pop() {
        println!("{}", top);
    }
}
