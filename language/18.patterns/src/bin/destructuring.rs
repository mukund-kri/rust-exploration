/// Lets explore destructuring irrefutable patterns

/// The let statement is a pattern matching statement
/// syntax is let <pattern> = <expression>

/// Model a point in 2D space
struct Point {
    x: i32,
    y: i32,
}

/// Model's a line in 2D space
struct Line {
    start: Point,
    end: Point,
}

#[allow(unused_variables)]
fn main() {
    // PATTERNS WITH LET STATEMENTS
    println!("\nPATTERNS WITH LET STATEMENTS");
    // The pattern is matched against the expression. Here the right hand side is a single
    // atomic value and cannot be deconstructed further. Hence the pattern is irrefutable only with
    // a single variable
    let x = 5;

    println!("\nDeconstructing tuples");
    // Now lets try to deconstruct a tuple.
    // Note: to be irrefutable, the number of variables in the pattern should match the number of
    // elements in the tuple
    let (x, y) = (5, 6);
    println!("x = {}, y = {}", x, y);

    // not valid
    // let (x, y) = (5, 6, 7);

    println!("\nDeconstructing structs");
    // Now lets try to deconstruct a struct
    let origin = Point { x: 0, y: 0 };

    let Point { x: xval, y: yval } = origin;
    println!("x = {}, y = {}", xval, yval);

    // syntax sugar
    let Point { x, y } = origin;
    println!("x = {}, y = {}", x, y);

    // Deconstructing works with nested structs (of arbitrary depth) as well
    let line = Line {
        start: Point { x: 0, y: 0 },
        end: Point { x: 1, y: 1 },
    };
    let Line {
        start: Point {
            x: startx,
            y: startyy,
        },
        end: e,
    } = line;
    println!(
        "startx = {}, startyy = {}, end.x = {}, end.y = {}",
        startx, startyy, e.x, e.y
    );

    // It follows that we can deconstruct arrays as well
    println!("\nDeconstructing arrays");

    // Note: the number of variables in the pattern should match the number of elements in the array
    let a = [1, 2, 3];
    let [a1, a2, a3] = a;
    println!("a1 = {}, a2 = {}, a3 = {}", a1, a2, a3);

    // not valid
    // let [a1, a2] = a;

    // If we need the only first x elements, we can use the rest operator
    // Its very similar to the spread operator in javascript
    let [a1, ..] = a; // In this case only the first element is bound to a1, the rest are ignored
    println!("a1 = {}", a1);

    // Similarly we can use the rest operator to ignore the first x elements
    let [.., a3] = a; // In this case only the last element is bound to a3, the rest are ignored
    println!("a3 = {}", a3);

    // We can use only 1 rest operator in a pattern
    // let [.., a3, ..] = a; // not valid

    // We can go further and name the elements captured by the rest operator
    let [head, tail @ ..] = a;
    println!("head = {}, tail = {:?}", head, tail);

    // This one is a bit wacky. Not only do we capture the last element, wl also capture a copy of
    // the whole array with @
    // This patten is built for the match statement, where we might need both the whole array and
    // a certain match
    let whole @ [.., last] = a;
    println!("whole = {:?}, last = {}", whole, last);

    // Patters work on all sorts of other language elements like strings, enums, etc. which I
    // ignore for now as the code will become too verbose
}
