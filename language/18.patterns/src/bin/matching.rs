/// Let's explore pattern matching
///
/// Copied from chapter 6: Enums and Pattern Matching from the Rust by Example book

/// Let's look at the basic syntax.
/// When used with primitive types like int / char / bool, it looks like a fancy
/// switch statement
fn basics() {
    println!("\nPattern Matching basics ... \n");
    let x = 1;
    match x {
        1 => println!("One"),                     // Match a single value
        2 | 3 | 5 | 7 | 11 => println!("Prime"),  // Match multiple values
        20..=40 => println!("Between 20 and 40"), // Match value in a range
        _ => println!("Did not match Anything"),  // Catch all. When none of the above match
    }

    // Note: the match MUST BE EXHAUSTIVE. Meaning, all possible values of the type must be
    // matched. If not, the compiler will throw an error.

    // Let's try another match, this time on a boolean
    let b = true;
    let s = match b {
        true => "Yes",
        false => "No",
    };
    println!("{} -> {}", b, s);
}

/// The real power of pattern matching is when it's used with complex type with
/// destructuring.

/// Let's look at an example of destructuring a tuple
/// This example is directly lifted from the Rust by Example book
fn tuples() {
    println!("\nPattern Matching with Tuples ... \n");
    let triple = (0, -2, 3);
    println!("The tuple under consideration is {:?}", triple);

    // when we use match with a tuple, it is destructured and then matched
    match triple {
        (0, y, z) => println!("First value is 0, y is {} and z is {}", y, z),
        (1, ..) => println!("First value is 1 and the rest don't matter"),
        (.., 2) => println!("Last value is 2 and the rest don't matter"),
        (3, .., -1) => println!("First value is 3, last value is -1 and the rest don't matter"),
        _ => println!("Does not match anything"),
    }
}

/// Let's do matching on arrays and slices.
/// This is exciting because I can do the kind of recursive algorithms that I learnt in scala/haskell
#[allow(unreachable_patterns)]
fn arrays_and_slices() {
    println!("\nPattern Matching with Arrays and Slices ... \n");
    // The basics of array matching
    let arr = [1, 2, 3];

    match arr {
        // Full array match
        [1, 2, 3] => println!("Array matches [1, 2, 3]"),
        [a, b, c] => println!("Array matches [{}, {}, {}]", a, b, c),

        // Match can also be ranges as per the array slice rules
        [1, ..] => println!("Array starts with 1"),
        [.., 3] => println!("Array ends with 3"),
        [1, .., 3] => println!("Array starts with 1 and ends with 3"),
        [1, .., 3] => println!("Array starts with 1 and ends with 3"),
        [1, .., -1] => println!("Array starts with 1 and ends with -1"),

        // And more interestingly, the @ sigil
        [first, tail @ ..] => println!("Array starts with {} and the rest is {:?}", first, tail),
        // Catch all
        _ => println!("Does not match anything"),
    }

    // Exploring the @ sigil in more detail
    let arr = [1, 2, 3, 4, 5];
    match arr {
        [first, tail @ ..] => println!("Array starts with {} and the rest is {:?}", first, tail),
        _ => println!("Does not match anything"),
    }
}

/// This allows me to do this, Very similar to haskell/scala
fn array_sum(arr: &[i32]) -> i32 {
    match arr {
        [first, tail @ ..] => first + array_sum(tail),
        _ => 0,
    }
}

#[allow(dead_code)]
enum Color {
    // These 3 are specified solely by their name.
    Red,
    Blue,
    Green,
    // These likewise tie `u32` tuples to different names: color models.
    RGB(u32, u32, u32),
    HSV(u32, u32, u32),
    HSL(u32, u32, u32),
    CMY(u32, u32, u32),
    CMYK(u32, u32, u32, u32),
}

/// Matching really shines when working with enums.
/// This code is a direct copy from the Rust by Example book
fn with_enums() {
    println!("\nPattern Matching with Enums ... \n");

    let color = Color::RGB(122, 17, 40);
    // TODO ^ Try different variants for `color`

    println!("What color is it?");
    // An `enum` can be destructured using a `match`.
    match color {
        Color::Red => println!("The color is Red!"),
        Color::Blue => println!("The color is Blue!"),
        Color::Green => println!("The color is Green!"),
        Color::RGB(r, g, b) => println!("Red: {}, green: {}, and blue: {}!", r, g, b),
        Color::HSV(h, s, v) => println!("Hue: {}, saturation: {}, value: {}!", h, s, v),
        Color::HSL(h, s, l) => println!("Hue: {}, saturation: {}, lightness: {}!", h, s, l),
        Color::CMY(c, m, y) => println!("Cyan: {}, magenta: {}, yellow: {}!", c, m, y),
        Color::CMYK(c, m, y, k) => println!(
            "Cyan: {}, magenta: {}, yellow: {}, key (black): {}!",
            c, m, y, k
        ),
        // Don't need another arm because all variants have been examined
    }
}

/// And, syntax for pattern matching on structs
struct Point {
    x: i32,
    y: i32,
}

fn with_structs() {
    println!("\nPattern Matching with Structs ... \n");

    let origin = Point { x: 0, y: 0 };

    match origin {
        Point { x: 0, y: 0 } => println!("(0,0) is the origin"),
        Point { x, y } => println!("({},{}) is not the origin", x, y),
    }
}

fn main() {
    basics();
    tuples();
    arrays_and_slices();

    let arr = [1, 2, 3, 4, 5];
    println!("Sum of array is {}", array_sum(&arr));

    with_enums();
    with_structs();
}
