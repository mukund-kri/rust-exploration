/// Let's explore patterns (destructuring) in functions and for loop

/// Model a point in 2D space
struct Point {
    x: i32,
    y: i32,
}

// Lets define a function that takes a Point struct as a parameter
fn print_coordinates(Point { x, y }: Point) {
    println!("x = {}, y = {}", x, y);
}

fn main() {
    // PATTERNS IN FUNCTION PARAMETERS
    println!("\nPATTERNS IN FUNCTION PARAMETERS");
    // Patterns can be used in function parameters as well

    // Now lets call the function
    let point = Point { x: 5, y: 6 };
    print_coordinates(point);

    // PATTERNS IN FOR LOOPS
    println!("\nPATTERNS IN FOR LOOPS");
    // using destructuring in for loops
    let points = vec![
        Point { x: 0, y: 0 },
        Point { x: 1, y: 1 },
        Point { x: 2, y: 2 },
    ];
    for Point { x, y } in points {
        println!("x = {}, y = {}", x, y);
    }
}
