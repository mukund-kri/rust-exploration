pub enum Coin {
    Penny,
    Nickle,
    Dime,
    Quarter,
}

pub fn value_in_cents(coin: Coin) -> i8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickle => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}
