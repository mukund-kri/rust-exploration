mod coin;

use self::coin::{value_in_cents, Coin};

// Basic enum example. IP address types
#[derive(Debug)]
enum IPAddress {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn option_example() {
    // Declare a few option variables
    let int_opt = Some(5);
    let string_opt = Some("A string");
    let int_opt2: Option<i32> = None;

    println!(
        "Examples of options {:?} - {:?} - {:?}",
        int_opt, string_opt, int_opt2
    );

    // Un-boxing the Some
    let int = 8;

    // How to extract the int value encapsulated in Some
    let sum = int + int_opt.unwrap_or(0);
    println!("Sum is {}", sum);

    // Now with a None value
    let sum = int + int_opt2.unwrap_or(0);
    println!("Sum is {}", sum);
}

fn main() {
    // A  ipv4 address
    let my_ip = IPAddress::V4(127, 0, 0, 1);
    println!("The IP address is {:?}", my_ip);

    // A ipv4 address
    let my_ip2 = IPAddress::V6(String::from("2001:db8:3333:4444:5555:6666:7777:8888."));
    println!("The IPv6 is {:?}", my_ip2);

    option_example();

    // Pattern matching example
    let my_coin = Coin::Quarter;
    let coin_value = value_in_cents(my_coin);
    println!("Value of my coin :: {}", coin_value);
}
