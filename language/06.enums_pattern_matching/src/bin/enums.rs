/// Exploring enums

/// A very basic example. This enum has only three simple variants
#[derive(Debug)]  // enums can also be derived from traits. More on this later.
enum BasicColor {
    Red,
    Green,
    Blue,
}

fn demo_basic_colors() {

    // Syntax for creating instances of enums. In the `BasicColor` case it is pretty straight 
    // forward
    let bc_red = BasicColor::Red;
    let bc_green = BasicColor::Green;
    let bc_blue = BasicColor::Blue;

    println!("Basic colors are {:?}, {:?}, {:?}", bc_red, bc_green, bc_blue);
}


/// Enums are far richer than the basic example above. They can have data associated with each
/// variant.
#[derive(Debug)]
enum MonoColor {
    Red(u8),
    Green(u8),
    Blue(u8),
}

fn demo_mono_color() {

    // Syntax for creating instances of enums values associated with each variant
    let mc_red = MonoColor::Red(255);
    let mc_green = MonoColor::Green(255);
    let mc_blue = MonoColor::Blue(255);

    println!("Mono colors are {:?}, {:?}, {:?}", mc_red, mc_green, mc_blue);
}

/// That's all nice but let's pick up a more realistic example. Let's say we can represent a
/// color as RGB value or a TSL value. We can use enums to represent this.
#[derive(Debug)]
enum Color {
    RGB(u8, u8, u8),    // The values look like tuples ie. un-named fields
    TSL(f32, f32, f32),
}

fn demo_color() {

    // Syntax for creating instances of enums values associated with each variant
    let c_red = Color::RGB(255, 0, 0);
    let example_tsl = Color::TSL(0.0, 0.5, 0.5);
    println!("Colors are {:?}, {:?}", c_red, example_tsl);
}

/// In this example from the book, we can see that variants can also have named fields, just
/// like structs. 
#[allow(dead_code)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

/// Enums can also have methods like structs
impl Message {
    fn print(&self) {
        match self {
            Message::Quit => println!("Quit"),
            Message::Move { x, y } => println!("Move to x: {}, y: {}", x, y),
            Message::Write(s) => println!("Write {}", s),
            Message::ChangeColor(r, g, b) => println!("Change color to r: {}, g: {}, b: {}", r, g, b),
        }
    }
}

fn demo_message() {
    let m = Message::Write(String::from("Hello"));
    m.print();

    let m = Message::Move { x: 10, y: 20 };
    m.print();
}

/// Enums can also have generic types like the Option<T> enum
#[allow(dead_code)]
enum Option<T> {
    Some(T),
    None,
}

/// Wow!!! enums are much more versatile than I thought.

fn main() {

    demo_basic_colors();
    demo_mono_color();
    demo_color();   
    demo_message();
}