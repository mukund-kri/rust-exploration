/// pattern matching has one more trick up its sleeve, Guards.
/// Guards are extra match conditions that can be added to filter the arm. This condition
/// can be any expression that returns a boolean value.


/// Point represents a point on a 2D coordinate system
struct Point {
    x: i32,
    y: i32,
}

/// Simple pattern matching with guards
fn with_guards() {
    println!("\nPattern Matching with Guards ... \n");

    let point = Point { x: -9, y: 4 };

    match point {
        Point { x: 0, y: 0 } => println!("On the origin"),

        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y: 0 } => println!("On the x axis at {}", x),

        Point { x, y } if x >= 0 && y >= 0 => println!("On the first quadrant"),
        Point { x, y } if x <= 0 && y >= 0 => println!("On the second quadrant"),
        Point { x, y } if x <= 0 && y <= 0 => println!("On the third quadrant"),
        Point { x, y } if x >= 0 && y <= 0 => println!("On the fourth quadrant"),
        _ => println!("Somewhere else"),  // Note this point is never reached, but the compiler 
                                          // cannot check the boundary conditions of guards
    }
}

/// Let's explore a more obscure feature of pattern matching, binding.
/// The following example is from the Rust by Example book
fn age() -> u32 {
    15
}

fn some_number() -> Option<u32> {
    Some(42)
}

fn binding_example() {

    match age() {
        0            => println!("I'm not born yet I guess"),
        n @ 1..=12 => println!("I'm a child of age {:?}", n), 
        // Note the variable carying the age is not directly accessible. But is bound to n with the @ sigil
        n @ 13..=19 => println!("I'm a teen of age {:?}", n),  // @ sigil again
        n           => println!("I'm an old person of age {:?}", n),  // default
    }

    // Binding also works with destructuring
    match some_number() {
        Some(n @ 42) => println!("The Answer: {}!", n),
        Some(n)      => println!("Not interesting... {}", n),
        _            => (),
    }
}

fn main() {
    with_guards();
    binding_example();
}