/// The `if let` is syntax sugar for cases where you want to match one pattern and ignore the rest.

fn main() {

    println!("Exploring the if let syntax ...");
    let some_number = Some(42);

    // the match way select for only one pattern and ignore the rest
    match some_number {
        Some(42) => println!("The Answer to life: {}!", 42),
        _        => (),
    }

    // the if let way is more concise
    if let Some(42) = some_number {
        println!("The Answer to life: {}!", 42);
    }

    // if let also let's us tag on an else branch too
    if let Some(42) = some_number {
        println!("The Answer to life: {}!", 42);
    } else {
        println!("Not interesting... {}", 42);
    }

    println!("\nExploring the while let syntax ...");
    // There also a while loop associated with this concept
    // The so called `while let` loop
    // Example from the rust by example book
    let mut optional = Some(0);

    loop {
        match optional {
            Some(i) => {
                if i > 4 {
                    println!("Greater than 4 quit!");
                    optional = None;
                } else {
                    println!("`i` is `{:?}`. Try again.", i);
                    optional = Some(i + 1);
                }
            },
            _ => { break; }
        }
    }

    // a more concise way to write this is with the `while let` loop
    let mut optional = Some(0);

    while let Some(i) = optional {
        if i > 4 {
            println!("Greater than 4, quit!");
            optional = None;
        } else {
            println!("`i` is `{:?}`. Try again.", i);
            optional = Some(i + 1);
        }
    }

}