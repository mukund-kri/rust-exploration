# 6: Enums and Pattern Matching


## Concepts
Enums gives us a way of saying the value is one of a limited set of values.

- Rust enums are similar to `case classes` in scala
- Made for Pattern Matching

## Syntax

Take for instance the following (simplest) enum  ...

```rust
enum Color {
    Red,
    Green,
    Blue,
}
```

Note the parts of this enum ...
- identifier :: Color 
- variants   :: This enum has 3 variants. Red, Green and Blue

**Instances**

We can declare instances of an enum like this ...

```rust
let red = Color::Red;
let blue = Color::Blue;
```

### Variants can have data associated with it


## Code

#### [main.rs](src/main.rs)

TODO: where does this fit??? It's not pattern matching, but it's not enums either ...

#### [enums.rs](src/bin/enums.rs)

Pattern matching on enums

#### [guards.rs](src/bin/guards.rs)

Pattern matching with guards

#### [if_let.rs](src/bin/if_let.rs)

The `if let` construct; when we only care about one of the cases

#### [matching.rs](src/bin/matching.rs)

Exploring the full pattern matching syntax over ...
1. Literals
2. Ranges
3. Tuples
4. Destructuring
5. Arrays and Slices
6. Enums
7. Structs
etc.