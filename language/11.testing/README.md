# Testing in Rust

Like all modern languages, Rust has a built-in testing framework (actually its
in cargo, but thats core rust). It's invoked with `cargo test`.

It supports unit and integration tests as separate entities. Unit tests are
defined in the same file as the code they test, and integration tests are 
defined in a separate directory.