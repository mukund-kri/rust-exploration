# Structs

Exploring structs in Rust.

## basic_example.rs

A basic example of a struct in Rust.

## book_example.rs

The example for structs from the Rust book.