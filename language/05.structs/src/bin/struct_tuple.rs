// Tuple structs are tuples (elements are not named) but the whole data type is named
// like structs

#[derive(Debug)]
struct Color(i32, i32, i32);

#[derive(Debug)]
struct Point(i32, i32, i32);

fn main() {
    // Using tuple structs

    // Instantiating tuple structs
    let c = Color(0, 0, 0);
    let p: Point = Point(0, 9, 10);

    print!("Your color: {:?}", c);
    print!("Your point: {:?}", p);

    // Destructuring tuple structs
    let Color(r, g, b) = c;

    println!("Red: {}, Green: {}, Blue: {}", r, g, b);

    // Accessing elements of a tuple struct
    println!("The first element of the Color is {}", c.0);
    println!("The second element of the Color is {}", c.1);
    println!("The third element of the Color is {}", c.2);
}
