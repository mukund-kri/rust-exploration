// Let's discover structs

// It a collection of fields.
#[derive(Debug)]
struct Person {
    first_name: String,
    last_name: String,
    age: u16,
}

// Structs can also have methods
impl Person {
    // This is a method
    fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }

    // Another method. This one pretty prints the person
    fn pretty_print(&self) {
        println!("{} is {} years old", self.full_name(), self.age);
    }

    // It can also have static methods, which are called associated functions
    fn new(first_name: &str, last_name: &str, age: u16) -> Self {
        Self {
            first_name: String::from(first_name),
            last_name: String::from(last_name),
            age, // shorthand for age: age
        }
    }
}

fn main() {
    // Let's create a person
    let john = Person {
        first_name: String::from("John"),
        last_name: String::from("Doe"),
        age: 23,
    };

    // We can print it because of the Debug trait
    println!("{:?}", john);

    println!("{}", john.full_name());

    // A struct can be destructured to its individual components
    let Person {
        first_name: fname,
        last_name: lname,
        age: a,
    } = john;
    println!("After destructuring :: {fname}, {lname}, {a}");

    println!("\nExploring associated functions");
    // Jane is our next person
    let jane = Person::new("Jane", "Doe", 21);
    jane.pretty_print();

    println!("\nExploring struct update syntax");
    // If Jane changes her last name, we can do that
    let jane = Person {
        last_name: String::from("Smith"),
        ..jane // This is called struct update syntax
    };
    jane.pretty_print();
}
