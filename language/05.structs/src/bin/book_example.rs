// The Rectangle type
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

// methods and associated functions are added to a struct in a `impl` block
impl Rectangle {
    // compute the area of the rectangle. Note the dot notation when calling this
    // method
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

impl Rectangle {
    // Associated methods are like static methods of c++. Note the double colon
    // calling notation for associated methods
    fn square(side: u32) -> Rectangle {
        Rectangle {
            width: side,
            height: side,
        }
    }
}

// Tuple structs are tuples (elements are not named) but the whole data type is named
// like structs

#[derive(Debug)]
struct Color(i32, i32, i32);

#[derive(Debug)]
struct Point(i32, i32, i32);

fn main() {
    let rect1 = Rectangle {
        width: 10,
        height: 20,
    };

    let rect2 = Rectangle {
        width: 9,
        height: 19,
    };

    let rect3 = Rectangle {
        width: 11,
        height: 21,
    };

    println!("The Rectangle struct {:?}", rect1);

    // Methods are called with the dot notation
    println!("The area of the rectangle {}", rect1.area());
    println!("Can rect1 hold rect2 {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect2 {}", rect1.can_hold(&rect3));

    // Create a Square with the square associated function
    let square = Rectangle::square(10);
    println!("Generated :: {:?}", square);

    //  TUPLE STRUCTS
    let c = Color(0, 0, 0);
    let p: Point = Point(0, 9, 10);

    print!("Your color: {:?}", c);
    print!("Your point: {:?}", p);

    // Destructuring tuple structs
    let Color(r, g, b) = c;

    println!("Red: {}, Green: {}, Blue: {}", r, g, b);

    // Accessing elements of a tuple struct
    println!("The first element of the Color is {}", c.0);
    println!("The second element of the Color is {}", c.1);
    println!("The third element of the Color is {}", c.2);
}
