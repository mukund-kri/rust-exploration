use macro_rules::say_hello;

fn main() {
    say_hello!();

    say_hello!("world");
}
