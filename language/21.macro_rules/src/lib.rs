// Arguments to a macro are prefixed with a dollar sign ($). The type is
// annotated with a designator.

#[macro_export]
macro_rules! say_hello {
    // When no argument is passed to `say_hello`, this will be the output
    () => {
        println!("Hello, world!");
    };
    // When a string is passed to `say_hello`, this will be the output.
    // Note the $ sign before the argument. And the expr designator.
    // The expr designator tells the macro that the argument is an expression.
    ($string: expr) => {
        println!("Hello, {}!", $string);
    };
}
