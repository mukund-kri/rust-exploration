mod chaining;
mod result_type;

use chaining::read_line_from_file;

fn main() {
    // plain_panic();

    // Demo panic backtracing
    // panic_call_trace();

    // Demo the Result type
    // play_with_result_type();

    // open_or_create_file2(String::from("hello.txt"));

    // Demo chaining
    let res = read_line_from_file(String::from("hello.txt"));
    println!("Result {:?}", res);
}

fn plain_panic() {
    // Prints an error message and exits the program
    panic!("Crash and burn");
}

fn panic_call_trace() {
    // Demo of RUST_BACKTRACE
    a();
}

fn a() {
    println!("In a");
    b();
}

fn b() {
    println!("In b");
    c(33);
}

fn c(arg: i32) {
    if arg == 33 {
        panic!("Don't pass 33 to this function")
    }
}
