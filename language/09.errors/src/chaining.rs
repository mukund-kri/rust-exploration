// Play with error chaining in rust

use std::fs::File;
use std::io;
use std::io::Read;

pub fn read_line_from_file(filename: String) -> Result<String, io::Error> {
    let mut line: String = String::new();

    File::open(&filename)?.read_to_string(&mut line)?;
    Ok(line)
}
