// Explore the result type
use std::fs::File;
use std::io::ErrorKind;

pub fn play_with_result_type() {
    let f = File::open("hello.txt");

    match f {
        Ok(_) => println!("File opened successfully"),
        Err(e) => println!("Failed to open with error {:?}", e),
    }
}

pub fn open_or_create_file(filename: String) {
    // First attempt of trying to open a file, and creating it if it does not exist
    // and read text from it.

    let _f = match File::open(&filename) {
        Ok(file) => file,
        Err(e) => match e.kind() {
            ErrorKind::NotFound => match File::create(&filename) {
                Ok(file) => file,
                Err(_e) => panic!("Error Creating file {}", &filename),
            },
            other_error => panic!("Problem :: {:?}", other_error),
        },
    };
}

pub fn open_or_create_file2(filename: String) {
    // Same as the function above but in this case use the unwrap_or_else
    // method.

    let _f = File::open(&filename).unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create(&filename).unwrap_or_else(|error| panic!("Problem :: {:?}", error))
        } else {
            panic!("Problem")
        }
    });
}
