# Chapter 10: Generics, Traits, and Lifetimes

This project roughly corresponds to the code in Chapter 10 of the book.


## files

1. `src/bin/basic.rs`: Absolute basics of generics
1. `src/bin/trait.rs`: Example from the book
2. `src/bin/trait_bounds.rs`: Generics but with bounds
3. `src/bin/lifetimes.rs`: Rust Lifetime annotation