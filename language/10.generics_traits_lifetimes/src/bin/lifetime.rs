/// Explore lifetimes generics in rust
///
/// Theory:
/// 1. All references have a `lifetime`
/// 2. This indicate how long a reference is valid for
/// 3. One of the `Borrow Checker's` job is to make validate lifetimes. ie. make sure
///    all the references are NOT invalidated before they are accessed.
/// 4. Most of this lifetime information is inferred by the `Borrow Checker`
///    automatically
/// 5. Some times there is not enough information in the code for the `Borrow Checker`
///    to infer lifetimes. In this cases we need to help the `Borrow Checker` with
///    `lifetime annotation`.
/// 6. Syntax:: lifetime annotation are denoted by a tic (') followed by a small letter

/// Example for a function with explicit lifetimes.
/// From the book.
fn longest<'a>(first: &'a str, second: &'a str) -> &'a str {
    if first.len() > second.len() {
        first
    } else {
        second
    }
}

fn main() {
    let f = "Hello";
    let s = "World!";
    let l = longest(f, s);

    // both f and s live until here hence the lifetime is valid
    println!("{}", l);

    let ff = "hello";
    let ll;
    {
        let x = String::from("World!");
        ll = longest(ff, x.as_str());
        println!("{}", ll);
    }

    // this will be an error as the lifetime of ll exceeds that of the x the second
    // parameter
    // print!("{}", ll);
}
