// Exploring the world of generics
use std::ops::Mul;

// A generic function
// Example given in the book
fn find_max<T: PartialOrd + Copy>(vec: Vec<T>) -> T {
    let mut largest: T = vec[0];
    for ele in vec {
        if ele > largest {
            largest = ele
        }
    }
    largest
}


// Generics can also be used with structs
#[derive(Debug)]
struct Rectangle<T: Copy> {
    length: T,
    height: T,
}

impl <T: Mul<Output = T> + Copy> Rectangle<T> {

    fn area(&self) -> T {
        self.length * self.height
    }
}

fn main() {

    let coll = vec!(1, 4, 456, 3, 2);
    let largest = find_max(coll);
    println!("{largest}");

    let coll3 = vec!('a', 'b', 'z', 'c');
    let largest = find_max(coll3);
    println!("{largest}");

    // Let's play with the generic struct
    
    // i32 rectangle
    let r1 = Rectangle{length: 20, height: 40};
    println!("{:?}", r1);
    println!("Area of rectangle :: {}", r1.area());
    println!();

    // Now let's do a f64 rectangle
    let r2 = Rectangle {length: 12.3, height:45.6};
    println!("Rectangle :: {:?}", r2);
    println!("Area of rectangle {}", r2.area());
    println!();


}
