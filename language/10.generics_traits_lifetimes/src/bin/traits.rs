// Let's explore traits.
// Basically traits enforce shared behavior in an abstract way.

// The trait Summary states that any struct that implements this trait must have
// summarize method.

trait Summary {
    // The no body implementation forces the implementing structs to provide a concrete
    // called summarize
    // fn summarize(&self) -> String;

    // We can also have default implementation
    fn summarize(&self) -> String {
        String::from("(Read More ...")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

// Now implement the trait
impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("headline: {} author: {}", &self.headline, &self.author)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Tweet {
    fn new(username: &str, content: &str, reply: bool, retweet: bool) -> Self {
        Self {
            username: String::from(username),
            content: String::from(content),
            reply,
            retweet,
        }
    }
}

// Now implement the Summary trait for Tweet
impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

// Now we can create a function that takes in a trait type instead of a concrete
// type
fn print_summary(summarizable: &impl Summary) {
    println!("{}", summarizable.summarize());
}

pub struct UnstructuredText {}

impl Summary for UnstructuredText {}

fn main() {
    let article = NewsArticle {
        headline: String::from("This is some headline"),
        location: String::from("Bangalore"),
        author: String::from("Nobody"),
        content: String::from("Some content of the article"),
    };

    println!("{}", article.summarize());

    // Article types can be sent to print_summary
    print_summary(&article);

    // Let's do a tweet
    let tweet = Tweet::new("@banglore_devs", "Welcome to our twitter", false, false);

    // This time let's call print_summary on a tweet
    print_summary(&tweet);

    // for `UnstructuredText` the default summarize should kick in
    let unstructured = UnstructuredText {};
    print!(
        "Summary of unstructured text :: {}",
        unstructured.summarize()
    );
}
