// Explore generics
#![allow(dead_code)]

// A generic function to find the largest element in a vector
fn get_largest<T: PartialOrd + Copy>(vec: Vec<T>) -> T {
    let mut largest: T = vec[0];

    for ele in vec {
        if ele > largest {
            largest = ele;
        }
    }
    largest
}

// Generics also work on data-types like structs and enums
#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let large1 = get_largest(vec![1, 4, 6, 2, 4]);
    println!("Largest is {}", large1);

    // Declare point with inferred type
    let p1 = Point { x: 1, y: 3 };
    println!("First Point is {:?}", p1);

    // full syntax
    let p2: Point<i32> = Point { x: 1, y: 4 };
    println!("Second Point is {:?}", p2);
}
