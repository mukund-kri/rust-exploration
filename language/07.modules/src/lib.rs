pub fn a_func() {
    println!("Hello from a_func!");
}

// Let's define a bunch of functions organized into modules as defined in the book, It
// Has the following structure:
//
// crate
//  └── front_of_house
//      ├── hosting
//      │   ├── add_to_waitlist
//      │   └── seat_at_table
//      └── serving
//          ├── take_order
//          ├── serve_order
//          └── take_payment

pub mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
        pub fn seat_at_table() {}
    }
    pub mod serving {
        pub fn take_order() {}
        pub fn serve_order() {}
        pub fn take_payment() {}
    }
}

fn deliver_order() {}

pub mod back_of_house {
    pub fn fix_incorrect_order() {
        cook_order();

        // Access the parent module using super::
        // note: the child module can access the parent module's private functions
        super::deliver_order();
    }
    fn cook_order() {}
}

/// Let's see how to use the functions defined in the modules
pub fn demo_modules() {
    // we can always use the full path
    crate::front_of_house::hosting::add_to_waitlist();

    // or we can use the use keyword to bring the item into scope
    use crate::front_of_house::hosting::seat_at_table;
    seat_at_table();

    // or we can use the use keyword to bring the module into scope
    use crate::front_of_house::serving;
    serving::take_order();

    back_of_house::fix_incorrect_order();
}
