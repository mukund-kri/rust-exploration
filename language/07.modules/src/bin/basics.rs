/// Explore crates and modules in rust

// import from lib.rs which is the root crate of this project
use modules::{a_func, demo_modules};

fn main() {
    a_func();

    demo_modules();
}