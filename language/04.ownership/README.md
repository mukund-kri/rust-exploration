# Ownership

Explore the basics of ownership in Rust.

## basics.rs

Explore the 4 types of ownership transfer in Rust.

## references.rs

Dig deeper into borrowing and references in Rust.

## slices.rs

A look at how ownership especially, borrowing work with slices in Rust.