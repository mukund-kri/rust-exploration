// shows the 4 basic rules of ownership in Rust

// 1. Moving
fn something_moved(_str: String) {
    // str is unallocated here
}

// 2. Coping
fn something_copied(_an_int: i32) {
    // No owner ship problems as an_int is a copy.
    // numbers and booleans parameters are copy ownership
}

// 3. Borrowing
fn something_borrowed(str: &String) {
    println!("We can use the borrowed {str}")
}

// 4. Mutable borrow
fn something_mutable_borrowed(str: &mut String) {
    // We can chang the we can mutate the string here and the changes will stick
    str.push_str("World!");
}

fn main() {
    println!("OWNERSHIP :: VARIABLES");

    // Create a new string
    let a_str = String::from("Hello ");
    println!("a_str is {}", a_str);

    // move a_str to b_str, this is where rust is different from other languages
    let b_str = a_str;
    // a_str is no longer valid as the ownership has been moved to b_str
    // println!("a_str is {}", a_str);
    // b_str is now the owner of the string object
    println!("b_str is {}", b_str);
    println!();

    println!("OWNERSHIP :: FUNCTIONS");
    println!("Move :: ownership");

    // Create a new string
    let a_str = String::from("Hello ");
    something_moved(a_str);

    // a_str has be de-allocated in something_moved, so error
    // println!("{}", a_str);

    println!("Copy :: ownership");
    let an_int: i32 = 56;
    something_copied(an_int);
    println!("an_int is still {an_int} because of copy ownership");

    // With borrowing the reference is pass around and the original is maintained
    println!("Borrowing :: ownership");
    let b_str = String::from("Hello");
    something_borrowed(&b_str);
    println!("{b_str} still exists in this scope");

    println!("Mutable borrow");
    let mut c_str = String::from("Hello ");
    something_mutable_borrowed(&mut c_str);
    println!("The mutably borrowed string is now {c_str}");
}
