// Let's go a little deeper into the references and borrowing.

// A simple function with borrowing
fn calculate_length(s: &String) -> usize {
    // Note &String instead of String. A reference is sent instead of the actual value.

    s.len()
}

/// Mutable references. A mutable reference allows to change the value of the reference.
/// But only one mutable reference can exist at a time.
///
/// # Arguments
/// s: &mut String - A mutable reference to a String that is going to be changed.
///
fn change(s: &mut String) {
    s.push_str(", world");
}

/// Dangling references. This is when a value goes out of scope but there is still a reference to it.
/// Following code will not compile.
// fn dangle() -> &String {
//     let s = String::from("Hello"); // s is a new String

//     &s // return a reference to the String, s
// }

/// On the flip side, we can return owned types from functions.
fn no_dangle() -> String {
    let s = String::from("Hello");

    s // return the String, s
}

fn main() {
    let s1 = String::from("Hello");
    let s2 = &s1; // s2 is a reference to s1

    // In this case s1 is not moved to s2, but s2 is a reference to s1.
    // s1 is still valid and can be used.
    println!("s1 = {}, s2 = {}", s1, s2);

    // The same goes for functions
    let s3 = String::from("Hello World");
    let len = calculate_length(&s3); // s3 is borrowed by the function
                                     // s3 is still valid and can be used.
    println!("The length of '{}' is {}.", s3, len);
    println!();

    // Mutable references
    let mut s4 = String::from("Hello");
    change(&mut s4); // s4 is borrowed as mutable reference
    println!("Now the value of s4 = {}", s4);

    // But only one mutable reference can exist at a time.
    let mut s5 = String::from("Hello");
    let r1 = &s5; // no problem
    let r2 = &s5; // no problem

    // let r3 = &mut s5; // BIG PROBLEM. Cannot borrow s5 as mutable because it is already borrowed as immutable.
    println!("Original :: {} - References :: {}, and {}", s5, r1, r2);

    println!();

    println!("Dangling references are caught at runtime in Rust.");
    // Owned types are returned from functions with no problem.
    let s5 = no_dangle();
    println!("s5 = {}", s5);
}
