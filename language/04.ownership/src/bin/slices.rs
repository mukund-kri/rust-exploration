/// Slices are a special kind of reference type that can reference a contiguous sequence
/// of elements in a collection rather than the whole collection.
/// This means it holds two pointers the start and the end of the slice.

/// Returns the first word of the given string. The book's demo for slices.
/// # Arguments
/// s: &str - A string whose first word is to be returned.
/// # Returns
/// &str - A slice of the first word of the given string.
fn first_word(s: &str) -> &str {
    // Looping over byte arrays is far easier.
    let bytes = s.as_bytes();

    // Iterate over the byte array.
    for (i, &item) in bytes.iter().enumerate() {
        // If the current byte is a space, return the slice of the string from the start to the current index.
        if item == b' ' {
            return &s[0..i];
        }
    }

    // If no space is found, return the slice of the string from the start to the end.
    &s[..]
}

fn main() {
    let s = String::from("Hello, world!");

    // we can call on first_word with s. The return slice is valid as long as the
    // reference to s is valid.
    let word = first_word(&s);

    println!("The first word of '{}' is '{}'.", s, word);

    // Slices indexing syntax is similar python sub list syntax.
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    println!("The slice of {:?} from 1 to 3 is {:?}", a, slice);

    // implicit start and end indices
    let slice = &a[..3];
    println!("The slice of {:?} from start to 3 is {:?}", a, slice);

    let slice = &a[1..];
    println!("The slice of {:?} from 1 to end is {:?}", a, slice);

    // Slices also work with arrays
    let arr = [1, 2, 3, 4, 5];
    let slice = &arr[1..3];
    println!("The slice of {:?} from 1 to 3 is {:?}", arr, slice);
}
