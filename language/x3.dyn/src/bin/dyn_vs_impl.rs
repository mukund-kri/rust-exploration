#![allow(unused)]

/// Let's explore the difference between `dyn Trait` and `impl Trait`.
///
/// I learned about this from this blog post
///  https://www.ncameron.org/blog/dyn-trait-and-impl-trait-in-rust/.
///
/// The dyn portion is from the `basic.rs` example, and is contrasted with the impl code.

struct Sheep {}
struct Cow {}

/// Both sheep and cows are Animals
trait Animal {
    // Instance method signature
    fn noise(&self) -> &'static str;
}

/// Implement the `Animal` trait for `Sheep`.
impl Animal for Sheep {
    fn noise(&self) -> &'static str {
        "baaaaah!"
    }
}

/// Implement the `Animal` trait for `Cow`.
impl Animal for Cow {
    fn noise(&self) -> &'static str {
        "moooooo!"
    }
}

/// Dynamic dispatch works on function parameters
/// Note: dyn works only with references (&) or boxes (Box<>)!
fn make_animal_noise_dyn(animal: &dyn Animal) {
    println!("{}", animal.noise());
}

/// The generic `impl Trait` version of the above function.
/// Note: this is syntax sugar for - fn make_animal_noise_impl<T: Animal>(animal: &T)
fn make_animal_noise_impl(animal: &impl Animal) {
    println!("{}", animal.noise());
}

/// Returns some struct that implements Animal, but we don't know which one at compile time.
/// Note: need a Box in this case, because the size of the return type is unknown at compile time.
fn random_animal(random_number: f64) -> Box<dyn Animal> {
    if random_number < 0.5 {
        Box::new(Sheep {})
    } else {
        Box::new(Cow {})
    }
}

// Can't do this with impl Trait. The compiler can't determine the type of the return value at
// compile time. Since each branch of the if statement returns a different type. And can only
// determined at runtime.

fn main() {
    // Let's make some animals
    let dolly = Sheep {};
    let molly = Cow {};

    // Let's make some noise with the dynamic dispatch version
    // Note: only 1 function is needed for both Sheep and Cow
    make_animal_noise_dyn(&dolly);
    make_animal_noise_dyn(&molly);

    // In the generic case 2 functions are generated, one for each type
    make_animal_noise_impl(&dolly);
    make_animal_noise_impl(&molly);
}
