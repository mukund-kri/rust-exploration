/// Example from the `Rust by Example` book.

struct Sheep {}
struct Cow {}

/// Both sheep and cows are Animals
trait Animal {
    // Instance method signature
    fn noise(&self) -> &'static str;
}

/// Implement the `Animal` trait for `Sheep`.
impl Animal for Sheep {
    fn noise(&self) -> &'static str {
        "baaaaah!"
    }
}

/// Implement the `Animal` trait for `Cow`.
impl Animal for Cow {
    fn noise(&self) -> &'static str {
        "moooooo!"
    }
}

/// Dynamic dispatch works on function parameters
/// Note: dyn works only with references (&) or boxes (Box<>)!
fn make_animal_noise(animal: &dyn Animal) {
    println!("{}", animal.noise());
}

/// Returns some struct that implements Animal, but we don't know which one at compile time.
/// Note: need a Box in this case, because the size of the return type is unknown at compile time.
fn random_animal(random_number: f64) -> Box<dyn Animal> {
    if random_number < 0.5 {
        Box::new(Sheep {})
    } else {
        Box::new(Cow {})
    }
}

/// We can do dynamic dispatch on vars, field etc. also
struct Farm {
    animals: Vec<Box<dyn Animal>>,
}
impl Farm {
    fn farms_din(self: &Farm) {
        println!("On this farm, you'll hear:");
        for animal in self.animals.iter() {
            println!("{}", animal.noise());
        }
    }
}

fn main() {
    let random_number = 0.234;
    let animal = random_animal(random_number);
    println!(
        "You've randomly chosen an animal, and it says {}",
        animal.noise()
    );

    // passing a reference to the function
    let dolly = Sheep {};
    make_animal_noise(&dolly);

    // Make a farm and listen to the din
    let farm = Farm {
        animals: vec![Box::new(Sheep {}), Box::new(Cow {})],
    };
    farm.farms_din();
}
