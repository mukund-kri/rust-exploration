# dyn

* `dyn`` stand for dynamic dispatch.
* Designed for use with `trait objects`.
* `trait objects` are instances of structs that implement a trait.
* `trait objects` are `fat pointers` that contain a pointer to the data and a pointer 
   to the `vtable`.
* Why trait as type? Traits is not a type and hence cannot have values. But we sometimes
   need to use traits as types to indicate a `set of types` that implement the trait. 
* Why dyn? In the above case, the compiler has no way to know the size of the type. 
   So we need to use `dyn` to indicate that the `object` is created at runtime and
   the compiler should use `fat pointer` to store the `object`.
* Its a `double pointer`; one pointer to the data and one pointer to the `vtable`.
* `vtable` is a table of function pointers.
* dyn is always a reference; either `&dyn` or `Box<dyn>`.
* `dyn Trait` and `impl Trait` have some overlap in functionality they are fundamentally
   different in implementation.
* `dyn Trait` is a `trait object` and `impl Trait` is a `generic`.
