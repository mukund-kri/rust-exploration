# Advanced Rust

Exploring the more advanced features of Rust. The topics include:

1. Unsafe Rust
2. Advanced Traits
3. Advanced Types
4. Advanced Functions and Closures
5. Macros

## Unsafe Rust

Unsafe Rust is a set of features that allow us to bypass the compiler's checks on memory
safety. This is useful when we need to interact with code that is written in another 
language, or when we need to write our own code that is not possible to write in 
safe Rust.

It basically allows us to:

1. Dereference raw pointers
2. Call unsafe functions or methods
3. Access or modify a mutable static variable
4. Implement unsafe traits
5. Access fields of unions

### Dereferencing Raw Pointers

