/// Exploring the following advanced trait concepts:
/// - Associated types
/// - Default generic type parameters and operator overloading
/// - Fully qualified syntax for disambiguation
/// - Supertraits
/// - Newtype pattern

// Associated types example from the "Rust by Example" book
struct Container(i32, i32);

// A trait which checks if 2 items are stored inside of container.
// Also retrieves first or last value.
trait Contains {
    // Define generic types here which methods will be able to utilize.
    type A;
    type B;

    fn contains(&self, _: &Self::A, _: &Self::B) -> bool; // Explicitly requires `A` and `B`.
    fn first(&self) -> i32; // Doesn't explicitly require `A` or `B`.
    fn last(&self) -> i32; // Doesn't explicitly require `A` or `B`.
}

impl Contains for Container {
    // Specify what types `A` and `B` are.
    type A = i32;
    type B = i32;

    // True if the numbers stored are equal.
    fn contains(&self, number_1: &i32, number_2: &i32) -> bool {
        (&self.0 == number_1) && (&self.1 == number_2)
    }

    // Grab the first number.
    fn first(&self) -> i32 {
        self.0
    }

    // Grab the last number.
    fn last(&self) -> i32 {
        self.1
    }
}

// Note: the input type is a trait not a concrete type.
fn difference<C: Contains>(container: &C) -> i32 {
    container.last() - container.first()
}

fn assoc_type_example() {
    let number_1 = 3;
    let number_2 = 10;

    let container = Container(number_1, number_2);

    println!(
        "Does container contain {} and {}: {}",
        &number_1,
        &number_2,
        container.contains(&number_1, &number_2)
    );
    println!("First number: {}", container.first());
    println!("Last number: {}", container.last());

    println!("The difference is: {}", difference(&container));
}

// FULLY QUALIFIED SYNTAX FOR DISAMBIGUATION
// The following example is from the "Rust" book
trait Pilot {
    fn fly(&self);
}

trait Wizard {
    fn fly(&self);
}

struct Human;

impl Pilot for Human {
    fn fly(&self) {
        println!("This is your captain speaking.");
    }
}

impl Wizard for Human {
    fn fly(&self) {
        println!("Up!");
    }
}

impl Human {
    fn fly(&self) {
        println!("*waving arms furiously*");
    }
}

// Disambiguation on associated functions is a bit different than methods.
trait Animal {
    fn baby_name() -> String;
}

struct Dog;

impl Dog {
    fn baby_name() -> String {
        String::from("Spot")
    }
}

impl Animal for Dog {
    // Associated functions requiring disambiguation use fully qualified syntax
    fn baby_name() -> String {
        String::from("puppy")
    }
}

fn disambiguation_example() {
    let person = Human; // this instance of Human has 3 methods named fly associated with it

    // The syntax for calling the 3 fly methods is as follows:
    person.fly();
    Pilot::fly(&person);
    Wizard::fly(&person);

    // Lets explore the associated functions
    println!("A baby dog is called a {}", Dog::baby_name()); // Straightforward

    // Now lets try to call the associated function from the Animal trait
    // Ambiguous call to `baby_name`: multiple `baby_name` found
    // println!("A baby dog is called a {}", Animal::baby_name()); // Using fully qualified syntax

    // Now it's clear. baby_name from the trait Animal impl block for struct Dog is called
    println!("A baby dog is called a {}", <Dog as Animal>::baby_name()); // Using fully qualified syntax
}

// SUPERTRAITS
// The following example is from the "Rust" book

/// A trait prints an outline around the display text
trait OutlinePrint: std::fmt::Display {
    fn outline_print(&self) {
        let output = self.to_string();
        let len = output.len();

        println!("{}", "*".repeat(len + 4));
        println!("*{}*", " ".repeat(len + 2));
        println!("* {} *", output);
        println!("*{}*", " ".repeat(len + 2));
        println!("{}", "*".repeat(len + 4));
    }
}

struct Point {
    x: i32,
    y: i32,
}

// Display is needed as a supertrait to use OutlinePrint
impl std::fmt::Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // Write custom formatting logic here
        write!(f, "({}, {})", self.x, self.y)
    }
}

// will not work without the supertrait Display
impl OutlinePrint for Point {}

fn supertraits_example() {
    let point = Point { x: 1, y: 2 };

    point.outline_print();
}

fn main() {
    assoc_type_example();
    disambiguation_example();
    supertraits_example();
}
