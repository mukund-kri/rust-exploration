/// Let's explore "UNSAFE" Rust
use std::slice;

/// It's not necessary that all functions that have unsafe code are unsafe functions. They can
/// wrap unsafe code in a safe function.
/// This code is copied from the Rust book. (which is from the standard library)
fn split_at_mut(values: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = values.len();
    let ptr = values.as_mut_ptr();

    assert!(mid <= len);

    unsafe {
        (
            slice::from_raw_parts_mut(ptr, mid),
            slice::from_raw_parts_mut(ptr.add(mid), len - mid),
        )
    }
}

// From the "C" stdlib
extern "C" {
    fn abs(input: i32) -> i32;
}

// Static variables are global mutable variables. Global mutable variables are a really bad idea.
// Most of the bugs in C and C++ are because of global mutable variables.
// Rust also allows global mutable variables, but only in unsafe code.
static mut COUNTER: u32 = 0;

fn main() {
    println!("\nRAW POINTERS");

    // Let's create two raw pointers. One immutable and one mutable.
    // Note: We can create raw pointers in safe Rust, but we can't dereference them.
    let mut x = 10;

    let ptr_x = &x as *const i32;
    let mut_ptr_x = &mut x as *mut i32;

    // Can't dereference raw pointers in safe Rust
    // println!("ptr_x: {}", *ptr_x);

    unsafe {
        // We can dereference raw pointers in unsafe Rust
        println!("ptr_x: {}", *ptr_x);
        println!("mut_ptr_x: {}", *mut_ptr_x);
    }

    println!("\nCALLING UNSAFE FUNCTIONS");

    // let's create an unsafe function
    unsafe fn unsafe_function() {
        println!("This is an unsafe function");
    }

    // can't call unsafe functions in safe Rust
    // unsafe_function();

    // But can in unsafe Rust
    unsafe {
        unsafe_function();
    }

    // cal call split_at_mut in safe code
    let mut values = [1, 2, 3, 4, 5];
    let (left, right) = split_at_mut(&mut values, 2);
    println!("left: {:?}, right: {:?}", left, right);

    // When call form foreign languages, we need to use unsafe
    unsafe {
        println!("abs(-3) from C: {}", abs(-3));
    }

    println!("\nACCESSING OR MODIFYING A MUTABLE STATIC VARIABLE");

    // Even accessing a static variable is considered unsafe
    // println!("COUNTER: {}", COUNTER );

    // To access we need an unsafe block
    unsafe {
        println!("COUNTER: {}", COUNTER);

        // We can also modify it
        COUNTER += 1;

        println!("COUNTER: {}", COUNTER);
    }
}
