/// The where clause can also be used to apply bounds in where clauses instead of using
/// the <Type: Trait> syntax.

/// The Point type
#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}
impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "(x: {}, y: {})", self.x, self.y)
    }
}

fn print_and_debug<T>(t: &T)
where
    T: Display + Debug, // Much cleaner syntax than <T: Display + Debug>
{
    println!("Debug: `{:?}`", t);
    println!("Display: `{}`", t);
}

fn main() {
    let string = "words";
    let point = Point { x: 1, y: 2 };

    print_and_debug(&string);
    print_and_debug(&point);
}
