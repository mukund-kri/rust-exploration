/// Explore the impl Trait syntax

trait Animal {
    fn name(&self) -> &'static str;
    fn talk(&self) {
        println!("{} cannot talk yet", self.name());
    }
}

#[derive(Debug)]
struct Human {
    name: &'static str,
}
impl Animal for Human {
    fn name(&self) -> &'static str {
        self.name
    }
    fn talk(&self) {
        println!("{} says hello", self.name());
    }
}

#[derive(Debug)]
struct Dog {
    name: &'static str,
}
impl Animal for Dog {
    fn name(&self) -> &'static str {
        self.name
    }
    fn talk(&self) {
        println!("{} says woof", self.name());
    }
}

// The following 2 functions are exactly the same.

/// Just a wrapper around animal.talk()
fn make_animal_talk1<T: Animal>(animal: &T) {
    animal.talk();
}

/// Exactly the same as make_animal_talk1() but using impl Trait syntax. The impl trait syntax
/// is just syntax sugar for the above function.
fn make_animal_talk2(animal: &impl Animal) {
    animal.talk();
}

/// We can also return `impl Trait` from a function.
fn create_human(name: &'static str) -> impl Animal {
    Human { name }
}

/// It does not support dynamic dispatch. The if statement branches the code into 2 with
/// each returning a different type and will not work even though the 2 types implement the
/// same trait.
// fn create_animal(name: &'static str, is_human: bool) -> impl Animal {
//     if is_human {
//         Human { name }
//     } else {
//         Dog { name }
//     }
// }

/// The correct way to do it is to use Box<dyn Trait> which is dynamic dispatch.
fn create_animal(name: &'static str, is_human: bool) -> Box<dyn Animal> {
    if is_human {
        Box::new(Human { name })
    } else {
        Box::new(Dog { name })
    }
}

fn main() {
    let human = Human { name: "John" };
    make_animal_talk1(&human);
    make_animal_talk2(&human);

    let human = create_human("John");
    make_animal_talk2(&human);

    // Dynamic dispatch
    let _dog = create_animal("Rusty", false);
}
