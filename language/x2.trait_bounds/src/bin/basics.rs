/// Exploring trait bounds in Rust
use std::fmt::{Debug, Display};

// Trait bounds are a way to restrict the generic types to only those types that have a particular
// behavior (implement a trait).

/// This function only accepts types that implement the `Display` trait
fn print<T: Display>(t: T) {
    println!("{}", t);
}

// This function only accepts types that implement `Debut` trait
fn debug<T: Debug>(t: T) {
    println!("{:?}", t);
}

/// This type is not printable nor debug-able
struct UnPrintable(i32);

/// This is printable
struct Printable(i32);
impl Display for Printable {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Printable({})", self.0)
    }
}

/// And this is debug-able  
#[derive(Debug)]
struct Debugable(i32);

fn main() {
    // let up = UnPrintable(10);
    // print(up);
    // debug(up);
    // Uncomment ^ to see the compiler error

    let p = Printable(20);
    print(p);

    let d = Debugable(30);
    debug(d);
}
