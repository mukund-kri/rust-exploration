use std::fmt::{Debug, Display};

fn compare_prints<T: Debug + Display>(t: &T) {
    println!("Debug: `{:?}`", t);
    println!("Display: `{}`", t);
}

/// Point represents a point in 2D space.
/// In this case I am going to implement the Display and Debug traits
#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}
impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "(x: {}, y: {})", self.x, self.y)
    }
}

fn main() {
    let string = "words";
    // let array = [1, 2, 3];
    // let vec = vec![1, 2, 3];

    compare_prints(&string);
    // compare_prints(&array); // won't compile, arrays don't implement Display
    // compare_prints(&vec); // also won't compile, vectors don't implement Display

    // On custom types
    let point = Point { x: 1, y: 2 };
    compare_prints(&point);
}
