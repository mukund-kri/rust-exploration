# Exploring Smart Pointers

## 1. Introduction

1. Smart pointers are data structures that act like pointers but also have additional metadata and capabilities. 
2. The additional capabilities are often related to memory management.
3. Data is stored on the heap.
4. Smart pointers implement the `Deref` and `Drop` traits.


## Code

1. `src/bin/box.rs` :: `Box<T>` is a smart pointer example
2. `src/bin/deref.rs` :: `Deref` trait example
3. `src/bin/drop_trait.rs` :: `Drop` trait example
4. `src/bin/rc.rs` :: `Rc<T>` smart pointer example