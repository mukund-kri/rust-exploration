// A struct as a placeholder of a smart pointer. To demonstrate the Drop trait.
struct CustomSmartPointer {
    data: String,
}

// Implementing the Drop trait for the struct.
impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

fn main() {
    // Creating a new instance of the struct.
    let c = CustomSmartPointer {
        data: String::from("my stuff"),
    };
    // The struct is dropped at the end of the scope.
    let _d = CustomSmartPointer {
        data: String::from("other stuff"),
    };
    println!("CustomSmartPointers created.");
    // The struct is dropped at the end of the scope.

    // We can't call the drop method directly.
    // c.drop();

    // But we can use the std::mem::drop function. To do the same thing.
    use std::mem::drop;
    drop(c);
}

