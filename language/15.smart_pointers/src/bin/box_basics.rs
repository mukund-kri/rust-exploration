// Explore the basics of the Box type

// It is a pointer type that is used to store data on the heap. This is useful when the size of the
// data is not known at compile time.
// The Box type is a smart pointer type.
use std::mem;

fn main() {
    // Create a box with an int in it. And then dereference it to get the value.
    println!("\n::: Box Basics :::");
    let a: Box<i32> = Box::new(5);
    println!("The value of a is :: {:?}", a);
    println!("The dereferenced value of a is :: {:?}", *a);

    // Boxes can also be mutable
    println!("\n::: Mutable Box :::");
    let mut b: Box<i32> = Box::new(10);
    println!("The value of b is :: {:?}", b);
    *b = 15;
    println!("The value of b after mutation is :: {:?}", b);

    // Memory allocation and size
    println!("\n::: Memory Allocation :::");
    let c: Box<i32> = Box::new(20);
    // Size of value 20 on the stack
    let num = 20;
    println!("Size of num is :: {:?}", mem::size_of_val(&num)); // 4 bytes, size of i32
    println!("Size of c is :: {:?}", mem::size_of_val(&c)); // 8 bytes, size of a pointer
}
