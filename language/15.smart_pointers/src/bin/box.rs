use std::ops;

trait Vehicle {
    fn drive(&self);
}

struct Truck;

impl Vehicle for Truck {
    fn drive(&self) {
        println!("Truck is Driving!");
    }
}

// Node in a linked list
// Note :: If the Box type in not implemented on the next field, rust will complain that Node
// is of infinite size. So we add Box as an indirection
#[derive(Debug)]
struct Node<T> {
    payload: T,
    next: Option<Box<Node<T>>>,
}

// Same example as above but with enums. Which makes it much better.
// Note :: The box is still needed or it will count as an infinite type.
#[derive(Debug)]
enum List<T> {
    Cons(T, Box<List<T>>),
    Nil,
}

fn main() {
    // Use case 1 for box. When we declare a variable of type Trait instead of a concrete type
    let a_vehicle: Box<dyn Vehicle>;
    a_vehicle = Box::new(Truck);
    a_vehicle.drive();

    // Use case 2 for box. When the size of the type is not know at compile time, like recursive
    // types.
    // Let's create a list
    let end = Node {
        payload: 5,
        next: None,
    };
    let lst = Node {
        payload: 11,
        next: Some(Box::new(end)),
    };
    println!("List :: {:?}", lst);

    // With the list type
    use List::{Cons, Nil};

    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
    println!("The list is :: {:?}", list);
}
