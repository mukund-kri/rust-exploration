/// Exploring the `Rc<T>` type.
/// Rc stands for Reference Counted. It is a type that keeps track of the number of references to
/// a value. It is used when we want to allocate some memory on the heap but we want to share it
/// across multiple parts of our program and we can't determine at compile time which part will
/// finish using the data last.
use std::rc::Rc;

use crate::List::{Cons, Nil};

#[derive(Debug)]
enum List<T> {
    Cons(T, Rc<List<T>>),
    Nil,
}

fn main() {
    // Let's create a list of numbers
    let list = Cons(1, Rc::new(Cons(2, Rc::new(Cons(3, Rc::new(Nil))))));
    println!("list = {:?}", list);

    let list = Rc::new(list);
    println!("list = {:?}", list);

    // Let's create a new list that shares the tail of the first list
    let list2 = Cons(4, Rc::clone(&list));
    println!("list2 = {:?}", list2);
    println!("Reference count of list = {}", Rc::strong_count(&list));

    // Let's create a third list that shares the tail of the first list
    let list3 = Cons(5, Rc::clone(&list));
    println!("list3 = {:?}", list3);
    println!("Reference count of list = {}", Rc::strong_count(&list));

    // Let's drop list2
    drop(list2);
    println!(
        "Reference count of list after list2 goes out of scope = {}",
        Rc::strong_count(&list)
    );

    // Let's drop list3 and check the count
    drop(list3);
    println!(
        "Reference count of list after list goes out of scope = {}",
        Rc::strong_count(&list)
    );
}
