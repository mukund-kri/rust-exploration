# Closures and Anonymous Functions

This code corresponds to the rust book chapter 13.

## Closures

1. Closures are anonymous functions.
2. We can assign closures to variables and use them as arguments to other functions.
3. Closures can capture values from the scope in which they're defined.

## Iterators

1. This pattern is implemented in most modern languages.
2. It allows us to perform some operation on a sequence of items in turn.
2. Iterators are lazy, meaning they have no effect until you call methods that consume the iterator to use it up.
3. the `Iterator` trait is core of iterators in rust.
4. the `next` method is the only required method of the `Iterator` trait.
