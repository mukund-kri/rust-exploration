/// Let's explore the basics. Syntax and Semantics.

/// Example of a function that can take in a closure.
/// The type signature of closures are more involved. This syntax is derived from the
/// fact that all closures derive from the Fn* traits.
fn closure_runner<F>(f: F, param: i32) -> i32
where
    F: Fn(i32) -> i32,
{
    f(param)
}

fn main() {
    println!("Closure Syntax ...");

    // Closures are anonymous functions that can be assigned to variables
    let plus_one = |x| x + 1;
    assert_eq!(2, plus_one(1));

    let ans1 = closure_runner(plus_one, 10);
    assert_eq!(11, ans1);

    // Closures have many syntax forms based on type annotations

    // Fully type inferred, shortest form.
    let c1 = |x| x + 1;
    println!("Calling c1. = {}", c1(10));

    // for bigger bodies
    let c2 = |x| {
        let inc = x + 1;
        inc
    };
    println!("Calling c2. = {}", c2(10));

    // With full type annotation
    let c3 = |x: i32| -> i32 { x + 1 };
    println!("Calling c3. = {}", c3(10));

    // For reference a function that does the same thing
    fn f1(x: i32) -> i32 {
        x + 1
    }
    println!("Calling function f1. = {}", f1(10));

    println!("\nClosures and Ownership\n");
    // Rust has this ownership thing and that opens a whole can of worms with closures
    // There are 3 ways a closure can access variables from the environment
    // 1. borrowing immutably
    // 2. borrowing mutably
    // 3. taking ownership

    // Borrowing immutably
    // This is the easy to reason about. Since the variable never changes, it can safely
    // be accessed from any number of closures for any number of times.
    println!("Borrowing immutably");
    let list = vec![1, 2, 3];
    println!("Before defining closure: list = {:?}", list);

    let c1 = || {
        println!("Inside c1: list = {:?}", list);
    };

    println!("Before calling c1: list = {:?}", list);
    c1();
    println!("After calling c1: list = {:?}", list);

    println!("\nBorrowing mutably");
    // When borrowing mutably, the same rules of borrowing apply. Only one mutable borrow
    // can exist at a time.

    let mut list = vec![1, 2, 3];
    println!("Before defining closure: list = {:?}", list);

    let mut c1 = || {
        println!("Inside c1: list = {:?}", list);
        list.push(4);
    };

    c1(); // This is fine as the mutable borrow dies with the last use of c1
    println!("After calling c1: list = {:?}", list);

    // but this is not OK!!! as c1 is still alive and list is borrowed mutably
    // c1();

    println!("\nTaking ownership");
    // move ownership can also be implemented in closures.
    // we use the `move` keyword to do this.
    let list = vec![1, 2, 3];
    println!("Before defining closure: list = {:?}", list);
    let c2 = move || {
        println!("Inside c2: list = {:?}", list);
    }; // list is moved into the closure here
    c2(); // when the clousre is called, list is moved into the closure and dies after call

    // this is an error as list is moved
    // println!("After calling c2: list = {:?}", list);
    // This pattern is commonly used in threads to move ownership of variables into the
    // thread.
}
