/// Let's explore iterators in Rust.

fn main() {
    // Let's make a vector of numbers, convert it to an iterator, and then loop over it
    // using a for loop.
    let numbers = vec![1, 2, 3, 4, 5];
    let numbers_iter = numbers.iter();
    for item in numbers_iter {
        println!("Item: {}", item);
    }

    // Let's prove that numbers implements the Iterator trait by repeatedly calling
    // next() on it.
    let numbers = vec![1, 2, 3];

    // Note that we need to use mut here because next() mutates the iterator.
    let mut numbers_iter = numbers.iter();

    assert_eq!(numbers_iter.next(), Some(&1));
    assert_eq!(numbers_iter.next(), Some(&2));
    assert_eq!(numbers_iter.next(), Some(&3));
    assert_eq!(numbers_iter.next(), None);

    // calling next() uses up the iterator, so we can't use it again.
    // Nothing will be printed here.
    for item in numbers_iter {
        println!("Item: {}", item);
    }

    // Similarly the ancillary methods on iterators also use up the iterator.
    // Let's create a new iterator and use the sum() method on it.
    let numbers_iter = numbers.iter();

    let sum: i32 = numbers_iter.sum();
    println!("Sum of numbers: {sum}");

    // Doing this again will result in an error because the iterator has been used up.
    // Uncomment the following lines to see the error.
    // let sum: i32 = numbers_iter.sum();

    // Methods that produce other iterators.
    // Of particular interest are methods the consume the iterator and produce a new one.
    // which we can use to program in a functional style, by chaining iterators together.

    // Let's consider the map() method, which takes a closure and applies it to each
    let numbers = vec![1, 2, 3, 4, 5];
    let squares: Vec<i32> = numbers.iter().map(|x| x * x).collect();
    println!("Squares: {:?}", squares);

    // Let's take a vector of numbers, filter out the odd ones, and then square them.
    let numbers = vec![1, 2, 3, 4, 5];
    let ans = numbers
        .iter()
        .filter(|x| *x % 2 == 0)
        .map(|x| x * x)
        .collect::<Vec<i32>>();
    println!("ans: {:?}", ans);
}

// Show capturing the environment in closures with tests. This example is straight from the Rust book.
#[derive(PartialEq, Debug)]
struct Shoe {
    size: u32,
    style: String,
}

fn shoes_in_my_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
    // The filter() method takes a closure that takes a reference to an item in the iterator
    // and returns a boolean. If the boolean is true, the item is included in the new iterator.
    shoes.into_iter().filter(|s| s.size == shoe_size).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn filter_by_size() {
        let shoes = vec![
            Shoe {
                size: 10,
                style: String::from("sneaker"),
            },
            Shoe {
                size: 13,
                style: String::from("sandal"),
            },
            Shoe {
                size: 10,
                style: String::from("boot"),
            },
        ];

        // The filter() method takes a closure that takes a reference to an item in the iterator
        // and returns a boolean. If the boolean is true, the item is included in the new iterator.
        let in_my_size = shoes_in_my_size(shoes, 10);
        assert_eq!(
            in_my_size,
            vec![
                Shoe {
                    size: 10,
                    style: String::from("sneaker"),
                },
                Shoe {
                    size: 10,
                    style: String::from("boot"),
                },
            ]
        );
    }
}
