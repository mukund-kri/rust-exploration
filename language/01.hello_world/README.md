# Rust ~hello world~ 

Here we have the traditional first program in a new language: Hello World!. There are
two version here.

## bare bone rust file

This is just the a file `just_rust/hello_world.rs` which can be compiled with the 
rust compiler `rustc` as so:

```bash

```bash
> rustc just_rust/hello_world.rs
```

This will create an executable file `hello_world`.


## cargo project

A more typical way to code in rust is to use the `cargo` tool. This is a build tool
and package manager for rust. It is similar to `npm` for nodejs or `pip` for python.

To create a new project with cargo, use the `new` command:

```bash
> cargo new hello_world
```

This will create a new directory `hello_world` with the following structure:

```bash
> tree hello_world
hello_world
├── Cargo.toml
└── src
    └── main.rs
```

We can run the code with cargo using the `run` command:

```bash
> cargo run
```