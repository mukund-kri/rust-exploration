/// Exploring the `Vector` collection type.

#[allow(unused_variables)]
fn main() {

    println!("Crating and initializing vectors ...");
    // Declaring and initializing vectors.
    let mut v1: Vec<i32> = Vec::new();

    // Vectors can also be created with the `vec!` macro.
    let v2 = vec![1, 2, 3];

    // Vectors can grow in size. We use the `push` method to add elements.
    v1.push(5);
    v1.push(6);
    v1.push(7);
    v1.push(8);

    println!("v1: {:?}", v1);

    println!("\nAccessing elements ...");
    // There are 2 ways to access the elements of a vector.
    // 1. With indexing.
    // 2. With the `get` method.

    // Indexing. Get the third element.
    // We get back a reference to the element. Accessing references is mostly a good idea due to
    // ownership rules.
    let third: &i32 = &v1[2];
    println!("The third element is {}", third);

    // `Get` API
    if let Some(fifth) = v1.get(4) {
        println!("The fifth element is {}", fifth);
    } else {
        println!("There is no fifth element.");
    }

    // Accessing non existent elements with indexing will panic.
    // let does_not_exist = &v1[100];

    // Of course, all the owenership rules apply to vectors as well.
    let first = &v1[0];
    v1.push(6); 
    // This will fail as we are trying to borrow `v1` as immutable and mutable at the same time.
    // println!("The first element is {}", first); 

    println!("\nIterating over vectors ...");
    for item in &v1 {
        println!("{}", item);
    }

    // Vector of enums
    #[derive(Debug)]
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    println!("\nVector of enums ...");
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Float(10.12),
        SpreadsheetCell::Text(String::from("Hello")),
    ];

    for item in &row {
        println!("{:?}", item);
    }

    // Droping the vector will drop all its elements.
    println!("\nDropping the vector ...");
    {
        let v = vec![1, 2, 3, 4];
        println!("v: {:?}", v);
    } // v goes out of scope and is dropped here. Also all its elements are dropped.
}