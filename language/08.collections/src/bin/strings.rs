/// Exploring rust `String` type
///
/// The core language has a `str` string type which is a string slice type. It is rather restrictive
/// so for most String operations/manuplation we use the `String` type from the std library
///
/// The String type is mutable, growable and owned so it far more versatile
/// It's so commonly used, it is automatically imported

fn main() {
    // Empty string
    let mut string0 = String::new();
    println!("{}", string0);

    // Strings can be appended to
    // push_str does NOT take ownership of input
    string0.push_str("Simple string");
    println!("{}", string0);

    // the push method appends chars instead of full strings
    string0.push('!');
    println!("{}", string0);

    // Simple string creation with values
    let string1 = String::from("Hello World1");
    println!("{}", string1);

    // OR directly from str
    let str2 = "Hello World2";
    let string2 = str2.to_string();
    println!("{}", string2);
    println!("{}", str2); // Again to_string does not take ownership

    println!("\nSome important methods ...");
    // + operator concatinats two strings
    let string3 = string0 + &string1;

    // Note: string0 now becomes un-usable as ownership is transferred
    // println!("{}, {}", string0, string1);
    println!("{}", string3);

    // The format! macro bring all the power of println! to create new strings from exiting ones
    let string4 = format!("My program has these strings :: {string1}, {string2} and {string3}");
    println!("{string4}");

    println!("\nIndexing strings ...");
    // Straight indexing like arrays and vectors are not supported
    // let ele = string3[0];

    // But slices are
    println!("First 4 chars of string1 is :: {}", &string1[0..4]);

    println!("\nIterating over String ...");
    // Iterating over chars in strings
    for char in string1.chars() {
        print!("{char}, ")
    }
    println!();

    // OR the byte version
    for byte in string1.bytes() {
        print!("{byte}, ");
    }
    println!();
}
