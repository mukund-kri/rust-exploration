/// Let's explore the hash map collection type.
/// It's a store of key-value pairs, where the keys are unique.
/// Give a key it uses a hash function to find the value.
// Since it is relatively rarely used it is not automatically imported.
use std::collections::HashMap;

fn main() {
    println!("Exploring HashMap ....");
    // Create a new hash map.
    let mut stationary_counts: HashMap<String, i32> = HashMap::new();

    // insert some values
    stationary_counts.insert(String::from("pens"), 10);
    stationary_counts.insert(String::from("pencils"), 50);

    println!("{:?}", stationary_counts);

    // Values are accessed with the keys. The get method is used for this mechanism
    // access the value given the key.
    // It returns an Option<V>. Some(value) if the key exists or None if the key does
    // not exists
    println!("\nAccessing Values ...");
    let item = String::from("pens");
    let non_existant = String::from("fountain-pen");

    println!("Number of pens: {:?}", stationary_counts.get(&item));
    println!(
        "Number of fountain pens: {:?}",
        stationary_counts.get(&non_existant)
    );

    // Get with default values
    let staplers = stationary_counts
        .get(&String::from("staplers"))
        .copied()
        .unwrap_or(0);
    println!("Number of Staplers {}", staplers);

    println!("\nLooping over a hash map ...");
    // Use for loop to iterate over hash maps
    for (key, value) in &stationary_counts {
        println!("Key: {}, Value: {}", key, value);
    }

    println!("\nOWNERSHIP (it's rust after all");
    let paper_clips = String::from("paper-clips");
    // Use the var paper_clips as a key
    stationary_counts.insert(paper_clips, 1000); // Ownership of the string is transferred to the map
                                                 // this throws an owner ship error

    // println!("Value of var paper_clips :: {paper_clips}");

    println!("\nUpdating values ...");
    let pen_key = String::from("pens");
    let notepad_key = String::from("note-pad");

    // Overwriting
    stationary_counts.insert(pen_key.clone(), 600);
    println!("{:?}", stationary_counts);

    // Inserting key-values only when key does not already exist
    // use the `entry` API
    stationary_counts.entry(pen_key.clone()).or_insert(40);
    stationary_counts.entry(notepad_key.clone()).or_insert(5000);
    println!("{:?}", stationary_counts);

    // Updating values based on old values, while checking for existence of key
    let count = stationary_counts.entry(notepad_key.clone()).or_insert(0);
    *count += 1;

    println!("{:?}", stationary_counts);
}
