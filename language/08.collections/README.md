# Collections

Exploring 3 important collection data structures in Rust. Strings, Vectors and Hash Maps.

#### [strings.rs](src/bin/strings.rs)

Exploring strings 

#### [vectors.rs](src/bin/vectors.rs)

Exploring vectors

#### [hash_maps.rs](src/bin/hash_maps.rs)

Exploring hash maps