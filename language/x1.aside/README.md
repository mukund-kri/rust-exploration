# Aside

Topics not covered or under covered in the Rust book.

## Table of Contents

1. [casting.rs](src/bin/casting.rs) :: Casting between types.
2. [dereferencing.rs](src/bin/dereferencing.rs) :: Dereferencing references.