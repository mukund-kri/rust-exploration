/// The Point struct models a point in 2D space
#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    // A primitive variable
    let a = 65;

    // A reference to `a`
    let b = &a;

    // check if the original variable and the dereferenced one are equal
    assert_eq!(a, *b);

    // Create a point and a reference to it
    let point = Point { x: 10, y: 20 };
    let ref_point1 = &point;

    // Not allowed as ref_point has shared ownership already
    // let _point2 = *ref_point1;

    // print the point and the dereferenced point
    // Note: the ref_point1 is dereferenced automatically in the print
    println!("Point: {:?}, Dereferenced Point: {:?}", point, ref_point1);

    // we can also explicitly dereference the reference
    println!("De-referenced Point: {:?}", *ref_point1);

    // Also the dot operator automatically dereferences the reference
    println!(
        "Point.x: {}, Dereferenced Point.x: {}",
        point.x, ref_point1.x
    );
}
