/// A look into casting in Rust
///
/// Note casting is not the same as converting. The rules largely follow C. And those not in C are
/// are fully described in Rust.
/// All casting is fully described in Rust.

fn main() {
    // There is NO implicit casting in Rust!
    let decimal = 65.4321_f32;

    // let integer: u8 = decimal; // This will not compile!

    // The way to do the above is by explicit casting with `as` keyword
    let integer = decimal as u8;
    println!("Casting {} -> {}", decimal, integer);

    // Non-sensical casting like float to char is not allowed
    // let character = decimal as char; // This will not compile!
}
