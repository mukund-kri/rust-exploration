use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: Sender<()>,
}

struct Worker {
    id: usize,
    thread: thread::JoinHandle<()>,
}

type Job = Box<dyn FnOnce() + Send + 'static>;

impl Worker {
    fn new(id: usize, receiver: Receiver<Job>) -> Worker {
        let thread = thread::spawn(|| {});

        Worker { id, thread }
    }
}

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// # Parameters
    ///
    /// * `_size`: The number of threads in the pool.
    ///
    /// # Returns
    ///
    /// A new ThreadPool.
    ///
    /// # Panics
    ///
    /// The `_size` parameter must be greater than zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        // First set up the channel through which the requests are routed.
        let (sender, _receiver) = channel();

        // Let's create the worker threads.
        let mut workers = Vec::with_capacity(size);
        for id in 0..size {
            let worker = Worker::new(id);

            workers.push(worker);
        }
        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, _f: F)
    where
        F: FnOnce() + Send + 'static,
    {
    }
}
