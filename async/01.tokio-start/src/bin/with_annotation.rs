// Hello world with tokio

// imports
use std::error::Error;

use log;
use log::Level::Info;
use tokio::time::{sleep, Duration};

async fn run() {
    log::info!("Starting sleep ...");
    sleep(Duration::from_secs(5)).await;
    log::info!("Finished sleep ...");
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    simple_logger::init_with_level(Info)?;
    run().await;

    Ok(())
}
