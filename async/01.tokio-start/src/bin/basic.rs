// My first tokio program

// imports
use std::error::Error;

use log;
use tokio::runtime::Runtime;
use tokio::time::{sleep, Duration};

async fn run() {
    log::info!("Starting sleep ...");
    sleep(Duration::from_secs(5)).await;
    log::info!("Finished sleep ...");
}

fn main() -> Result<(), Box<dyn Error>> {
    // initialize logger
    simple_logger::init_with_level(log::Level::Info)?;

    let rt = Runtime::new()?;

    let future = run();

    rt.block_on(future);

    Ok(())
}
