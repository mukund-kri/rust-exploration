use iced::widget::{button, column, text, Column};
use iced::Alignment;

fn main() -> iced::Result {
    iced::run("A Cool Counter", Counter::update, Counter::view)
}

#[derive(Default)]
struct Counter {
    value: i32,
}

#[derive(Debug, Clone, Copy)]
enum Message {
    Increment,
    Decrement,
}

impl Counter {
    fn update(&mut self, message: Message) {
        match message {
            Message::Increment => self.value += 1,
            Message::Decrement => self.value -= 1,
        }
    }

    fn view(&self) -> Column<Message> {
        column![
            button("Increment").on_press(Message::Increment),
            text(self.value.to_string()).size(50),
            button("Decrement").on_press(Message::Decrement),
        ]
        .padding(20)
        .align_items(Alignment::Center)
    }
}
