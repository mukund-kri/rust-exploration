use iced::widget::Text;
use iced::{Element, Result, Sandbox, Settings};

struct HelloUI;

impl Sandbox for HelloUI {
    type Message = ();

    fn new() -> Self {
        HelloUI
    }

    fn title(&self) -> String {
        String::from("Hello, world!")
    }

    fn update(&mut self, _message: Self::Message) {}

    fn view(&self) -> Element<Self::Message> {
        Text::new("Hello, world!").into()
    }
}
fn main() -> Result {
    HelloUI::run(Settings::default())
}
