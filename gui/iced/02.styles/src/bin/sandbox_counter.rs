use iced::widget::{button, column, text};
use iced::{Element, Sandbox, Settings};

// Define the counter state
#[derive(Default)]
struct Counter {
    value: i32,
}

#[derive(Debug, Clone)]
enum Message {
    Increment,
    Decrement,
}

impl Sandbox for Counter {
    type Message = Message;

    fn new() -> Self {
        Self { value: 0 }
    }

    fn title(&self) -> String {
        String::from("Counter")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            Message::Increment => {
                self.value += 1;
            }
            Message::Decrement => {
                self.value -= 1;
            }
        }
    }

    fn view(&self) -> Element<'_, Self::Message> {
        column![
            button("Increment").on_press(Message::Increment),
            text(self.value.to_string()).size(50),
            button("Decrement").on_press(Message::Decrement),
        ]
        .padding(20)
        .align_items(iced::Alignment::Center)
        .into()
    }
}

fn main() -> iced::Result {
    Counter::run(Settings::default())
}
