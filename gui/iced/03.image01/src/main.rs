// display a single image from file
use iced::widget::image::Image;
use iced::{Length, Sandbox, Settings};

const IMAGE_PATH: &str = "assets/rust.jpeg";

#[derive(Debug, Default)]
struct ImageViewer;

impl Sandbox for ImageViewer {
    type Message = ();

    fn new() -> Self {
        Self
    }

    fn view(&self) -> iced::Element<Self::Message> {
        Image::new(IMAGE_PATH)
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }

    fn update(&mut self, message: Self::Message) {
        println!("{:?}", message);
    }

    fn title(&self) -> String {
        String::from("Image Viewer")
    }
}
fn main() -> iced::Result {
    ImageViewer::run(Settings::default())
}
