mod ui;

use iced::{settings, Application, Result as IcedResult};

use ui::MyApp;

fn main() -> IcedResult {
    // initialize logging
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .init();

    // flags
    let paths = ui::Paths {
        notes_path: String::from("notes"),
        scans_path: String::from("scans"),
    };

    // default settings

    // custom settings
    let settings = settings::Settings {
        flags: paths,
        ..settings::Settings::default()
    };

    MyApp::run(settings)
}
