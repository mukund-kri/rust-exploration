use iced::widget::{button, column, text};
use iced::{executor::Default as IceExecutor, Application, Theme};

use log::info;

// The messages of our application
#[derive(Debug, Clone)]
pub enum Message {
    Plus,
    Minus,
}

// Flags / configuration for our application
#[derive(Debug, Clone, Default)]
pub struct Paths {
    pub notes_path: String,
    pub scans_path: String,
}

pub struct MyApp {
    count: i32,
    paths: Paths,
}

impl Application for MyApp {
    type Message = Message;
    type Executor = IceExecutor;
    type Flags = Paths;
    type Theme = Theme;

    fn new(flags: Self::Flags) -> (MyApp, iced::Command<Message>) {
        info!("Flags: {:?}", flags);
        (
            MyApp {
                count: 0,
                paths: flags,
            },
            iced::Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("MyApp")
    }

    fn update(&mut self, message: Message) -> iced::Command<Message> {
        match message {
            Message::Plus => {
                self.count += 1;
            }
            Message::Minus => {
                self.count -= 1;
            }
        }
        iced::Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, Self::Theme, iced::Renderer> {
        column![
            button("Increment").on_press(Message::Plus),
            text(self.count.to_string()).size(50),
            button("Decrement").on_press(Message::Minus),
        ]
        .padding(20)
        .into()
    }
}
